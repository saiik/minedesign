<?php
/*******************************************
 *	 _____ _____     _____ _____ _____     *
 *	|_   _|   __|___|     |     |   __|    *
 *	  | | |  |  |___|   --| | | |__   |    *
 *	  |_| |_____|   |_____|_|_|_|_____|    *
 *                                         *
 *  (C) by TG-Network and sLy(Tobias F.)   *
 *   Developed by sLy / SinetiX / Saphir   *
 *  Dont remove this.      (C) 2009-2010   *
 *******************************************/
	chdir('../');
	include('config/configMain.php');
	$smarty = new Smarty();
	$smarty->template_dir = '../templates/'.template().'/templates';
	$smarty->compile_dir = '../templates/'.template().'/templates_c';
	chdir('beta');
	
	include('ses.php');
if(empty($_GET["login"])) {
	if(!checkLogin()) {
		if(empty($_POST["submit"])) {
			$smarty->assign('error', '');
			$smarty->assign('dive', '');
			$smarty->assign('dive2', '');
			$smarty->display('login.tpl');
		} else {
			$error = array();
			
			if(empty($_POST["user"])) {
				$error[] = error('Bitte gebe ein Usernamen ein', 1);
			} else {
				$user = $_POST["user"];
			}
			if(empty($_POST["pass"])) {
				$error[] = error('Bitte gebe ein Passwort ein');
			} else {
				$pass = $_POST["pass"];
			}
			if(!empty($error)) {
				moveto("../index.php", 3, "Falsche Benutzerdaten!");
				$_SESSION["fail"] = $_SESSION["fail"] - 1;
			} else {
				$pass_new = '';
				for($i=0; $i<count($pass); $i++) {
					$pass_new = PW_HASH.$pass.PW_HASH;
				}
				$pass_new = md5($pass_new);
				$con = mysql_query("SELECT userID FROM tg_user WHERE Username = '".$user."' AND Passwort = '".$pass_new."'");
				if(mysql_num_rows($con)) {	
					$ds = mysql_fetch_assoc($con);
					$hash = md5(str_shuffle("sdfkjsdfvxksldfjslk.-"));
					$_SESSION["tg_Login"] = 1;
					$_SESSION["tg_Login_Hash"] = $hash;
					$_SESSION["tg_userID"] = $ds["userID"];
					$_SESSION["tg_User"] = $user;
					setcookie("tg_Login", 1, time()+3600); 
					setcookie("tg_Login_Hash", $hash, time()+3600); 
					setcookie("tg_userID", $ds["userID"], time()+3600); 
					setcookie("tg_User", $user, time()+3600); 
					// Who is Online..
					if($anz = mysql_num_rows(mysql_query("SELECT ID FROM tg_wio WHERE userID ='".$ds["userID"]."'"))) {
						$ip = $_SERVER['REMOTE_ADDR'];
						$time = time();
						$da = date('d.m.Y', $time);
						$timestamp = strtotime($da."+15 Minutes");
						$in = mysql_query("UPDATE tg_wio set userID = '".$ds["userID"]."', userIP = '".$ip."', userTIME = '".$time."', userOUT = '".$timestamp."'") or die(mysql_error());
						if($in) {
							moveto("../index.php", 0);
						}
					} else {
						$ip = $_SERVER['REMOTE_ADDR'];
						$time = time();
						$da = date('d.m.Y', $time);
						$timestamp = strtotime($da." +1 DAY");
						$in = mysql_query("INSERT INTO tg_wio (userID, userIP, userTIME, userOUT) VALUES ('".$ds["userID"]."', '".$ip."', '".$time."', '".$timestamp."')") or die(mysql_error());
						if($in) {
							moveto("../index.php", 0);
						}
					}
				} else {
					moveto("../index.php?content=login", 3, "Falsche Benutzerdaten!");
				}	
			}
			
		}
	} else {
		moveto("../index.php", 0);
	}
} elseif($_GET["login"] == "false") {
	session_destroy();
	unset($_SESSION["tg_Login"]);
	unset($_SESSION["tg_Login_Hash"]);
	unset($_SESSION["tg_userID"]);
	unset($_SESSION["tg_User"]);
	moveto("index.php", 0);
}
?>