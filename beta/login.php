<?php
/*******************************************
 *	 _____ _____     _____ _____ _____     *
 *	|_   _|   __|___|     |     |   __|    *
 *	  | | |  |  |___|   --| | | |__   |    *
 *	  |_| |_____|   |_____|_|_|_|_____|    *
 *                                         *
 *  (C) by TG-Network and sLy(Tobias F.)   *
 *   Developed by sLy / SinetiX / Saphir   *
 *  Dont remove this.      (C) 2009-2010   *
 *******************************************/
	include_once('config/configMain.php');
	include('beta/ses.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
		<style type="text/css">
			body { background-color: #333; }
			a, a:visited, a:hover { color: #FFF; text-decoration: none; }
			#Login { color: #FFF; margin-top: 15%; display: block; }
			.feld { font-family: arial; padding-left: 5px; font-size: 12px;	padding-top: 2px; padding-right: 5px; border: 1px #FFF solid; border-color: #8da4b7; height: 18px; width: 200px; -webkit-border-radius: 5px; -moz-border-radius: 5px; }
			.feld:focus { background-color: #fff9f4; border-color: #fa2; color: #333; }
			.errorbox {
			  background-color: #FFCCCC;
			  border: 1px dashed #D60000;
			  color: #666666;
			  font-size: 12px;
			  font-family: arial;
			  width: 100%;
			  padding: 8px;
			  text-align: left;
			  text-shadow: 0px 0px 0px #000;
			}
		</style>
		<title> TG-CMS | Beta-Login</title>
	</head>
<body>
	<div id="Login">
		<div align="center">
			<div style="width: 300px; text-align: left; text-shadow: 2px 5px 5px #000;">
			<?php if($_SESSION["fail"] >= 1) { ?>
				Sie betreten ein geschützen bereich.<br />
				Bitte Loggen Sie sich ein:<br />
				<hr size="1" style="border-bottom: 1px #000 solid;" />
				<form action="beta/checklogin.php" method="post">
					Username:<br />
					<dd><input type="text" class="feld" name="user" /></dd><br />
					Passwort:
					<dd><input type="password" class="feld" name="pass" /></dd>
					<hr size="1" style="border-bottom: 1px #000 solid;" />
					<br />
					<input type="submit" name="submit" value="Login" />
				</form>
			<?php if($_SESSION["fail"] <= 4) {
					echo '<br /><div class="errorbox">Sie haben noch '.$_SESSION["fail"].' Versuche sich einzuloggen</div>';
				} 
			} else { ?>
				Sie haben vesucht sich einzuloggen und 5 mal Falsche Daten eingeben.<br />
				Ihre IP-Adresse wird nun in unserer Datenbank gespeichert.
			<?php } ?>
			</div>
		</div>
	</div>
</body>
</html>