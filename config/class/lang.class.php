<?php
/*******************************************
 *#########################################*
 *##|_   _|   __|___|     |     |   __|####*
 *####| | |  |  |___|   --| | | |__   |####*
 *####|_| |_____|   |_____|_|_|_|_____|####*
 *#########################################*
 *  (C) by TG-Network and sLy(Tobias F.)   *
 * 		    This is a Free CMS             *
 *  Dont remove this.      (C) 2009-2010   *
 *******************************************/
 
	class Lang {
		var $file;
		var $lang = 'de';
		var $out = array();
		
		function read($out) {
			$this->file = 'language/'.$this->lang.'_'.$out.'.php';
			if(file_exists($this->file)) {
				include($this->file);
			} else {
				close('Language Datei nicht gefunden', 0);
			}

			foreach($lang as $key => $val) {
				$this->out[$key] = $val;
			}
		}
		
	}
 
?>