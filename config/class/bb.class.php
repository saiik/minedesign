<?php
/*******************************************
 *#########################################*
 *##|_   _|   __|___|     |     |   __|####*
 *####| | |  |  |___|   --| | | |__   |####*
 *####|_| |_____|   |_____|_|_|_|_____|####*
 *#########################################*
 *  (C) by TG-Network and sLy(Tobias F.)   *
 * 		    This is a Free CMS             *
 *  Dont remove this.      (C) 2009-2010   *
 *******************************************/

class BBCode {

	var $parser;

	function __construct($par) {
		$this->parser = $par;
	}

	/* FUNCTION BB PARSER
	 * BBCODE IN HTML TAGS UMWANDELN
	 */
	
	function BBParser($mes) {
		if($this->parser === 1) {
			$new = $this->BBCode($mes);
			$new = $this->flag($new);
			$new = $this->Quote($new);
			$new = $this->listul($new);
			$new = $this->spoiler($new);
			$new = $this->align($new);
			$new = $this->youtube($new);
			$new = $this->center($new);
			$new = $this->info($new);
			$new = $this->smil($new);
			return $new;
		} elseif($this->parser === 0) {
			return $mes;
		}
	}
	
	/* SIMPLE BBCODE
	 * [B] TEXT [/B]  ---- FETTER TEXT
	 * [I] TEXT [/I]  ---- KURISVER TEXT
	 * [COLOR=****] TEXT [/COLOR]  ---- FARBIGER TEXT
	 * [SIZE=**] TEXT [/COLOR]  ---- GR��E DES TEXTES
	 * [FONT=****] TEXT [/FONT] ---- FONT ART
	 * [URL=WWW] TEXT [/URL]  ---- URL LINK
	 */
	function BBCode($mes) {
		$mes = preg_replace('/\[b\](.*?)\[\/b\]/', '<b>$1</b>', $mes);
		$mes = preg_replace('/\[i\](.*?)\[\/i\]/', '<i>$1</i>', $mes);
		$mes = preg_replace('/\[B\](.*?)\[\/B\]/', '<b>$1</b>', $mes);
		$mes = preg_replace('/\[I\](.*?)\[\/I\]/', '<i>$1</i>', $mes);
		$mes = preg_replace('/\[u\](.*?)\[\/u\]/', '<u>$1</u>', $mes);
		$mes = preg_replace('/\[U\](.*?)\[\/U\]/', '<u>$1</u>', $mes);
		$mes = preg_replace('/\[left\](.*?)\[\/left\]/', '<div style="text-align: left;">$1</div>', $mes);
		$mes = preg_replace('/\[right\](.*?)\[\/right\]/', '<div style="text-align: right;">$1</div>', $mes);
		$mes = preg_replace('/\[center\](.*?)\[\/center\]/', '<div style="text-align: center;">$1</div>', $mes);
		$mes = preg_replace('/\[color=(.*?)](.*)\[\/color\]/', '<font style="color: $1">$2</font>', $mes);
		$mes = preg_replace('/\[size=(.*?)](.*)\[\/size\]/', '<font style="font-size: $1px">$2</font>', $mes);
		$mes = preg_replace('/\[font=(.*?)](.*)\[\/font\]/', '<font style="font-family: $1">$2</font>', $mes);
		$mes = preg_replace('/\[img\](.*)\[\/img\]/', '<img src="$1" alt="" />', $mes);
		$mes = preg_replace('/\[url=([^ ]+).*\](.*)\[\/url\]/', '<a href="$1">$2</a>', $mes);
		$mes = preg_replace('/\n/', "<br />\n", $mes);
		
		return $mes;
	}
	
	/* QUOTE BBCODE
	 * ZITATE WERDE FARBIG HERVORGEHOBEN
	 */
	
	function Quote($mes, $in="") {
		if(empty($in)) {
			$mes = rep('[quote]', '<div class="zitat">Zitat:<hr />', $mes);
			$mes = rep('[/quote]', '</div>', $mes);
			return $mes;
		} else {
			$mes = rep('[quote]', '<div class="zitat">Zitat von <i>'.$in.'</i>:<hr />', $mes);
			$mes = rep('[/quote]', '</div>', $mes);
			return $mes;
		}
	}
	
	
	function listul($mes) {
		$mess = rep('[list]', '<ul>', $mes);
		$mess = rep('[*]', '<li>', $mess);
		$mess = rep('[/*]', '</li>', $mess);
		$mess = rep('[/list]', '</ul>', $mess);
		
		return $mess;
	}
	
	
	/* FLAG ICONS
	 * [FLAG=XX] ---- FLAGEN ALS ICON
	 */
	
	function spoiler($link) {
		$mes = preg_replace('/\[spoiler=([^ ]+).*\](.*)\[\/spoiler\]/', '<a href="#" onclick="spoiler();">$1</a><br /><div id="spoiler">$2</div>', $link);
		return $mes;
	}
	function align($link) {
		$mes = preg_replace('/\[align=([^ ]+).*\](.*)\[\/align\]/', '<div style="text-align: $1;" >$2</div>', $link);
		return $mes;
	}
	
	function youtube($link) {
		$mes = preg_replace('/\[youtube\](.*?)\[\/youtube\]/', '<object width="425" height="344"><param name="movie" value="http://www.youtube.com/v/$1&hl=de&fs=1&"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/$1&hl=de&fs=1&" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="425" height="344"></embed></object>', $link);
		return $mes;
	}
	
	function center($link) {
		$mes = preg_replace('/\[center\](.*?)\[\/center\]/', '<center>$1</center>', $link);
		return $mes;
	}
	
	function smil($mes) {
		$mes = rep(':)', '<img src="templates/standart/images/sm/1.png" alt="" title=":)" />', $mes);
		$mes = rep(':D', '<img src="templates/standart/images/sm/4.png" alt="" title=":D" />', $mes);
		$mes = rep(':(', '<img src="templates/standart/images/sm/3.png" alt="" title=":(" />', $mes);
		$mes = rep(':P', '<img src="templates/standart/images/sm/5.png" alt="" title=":P" />', $mes);
		$mes = rep('<3', '<img src="templates/standart/images/sm/6.png" alt="" title="<3" />', $mes);
		$mes = rep('^^', '<img src="templates/standart/images/sm/7.png" alt="" title="^^" />', $mes);
		$mes = rep(';)', '<img src="templates/standart/images/sm/2.png" alt="" title=";)" />', $mes);
		
		return $mes;
	}
	
	function info($mes) {
		$mes = preg_replace('/\[info\](.*?)\[\/info\]/', '<div class="info">$1</div>', $mes);
		return $mes;
	}
	
	function flag($flagge) {
		$flagge = preg_replace('/\[flag=(.*?)]/', '<img src="templates/'.template().'/images/flags/$1.png" alt="" />', $flagge);	
		return $flagge;
	}
	
}
?>