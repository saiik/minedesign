<?php
/*******************************************
 *#########################################*
 *##|_   _|   __|___|     |     |   __|####*
 *####| | |  |  |___|   --| | | |__   |####*
 *####|_| |_____|   |_____|_|_|_|_____|####*
 *#########################################*
 *  (C) by TG-Network and sLy(Tobias F.)   *
 * 		    This is a Free CMS             *
 *  Dont remove this.      (C) 2009-2010   *
 *******************************************/
 
/* SESSION_START();
 *
 */
session_start();


/* DEBUG MODE ( 0 = OFF | E_ALL = ON) */

error_reporting(E_ALL & ~E_NOTICE);

/*	Passwort Hash
 *  Dont edit / Dont remove this!
 */
	
	define('PW_HASH', 'yxcyxcqewdsad3');
	
/*	Include MYSQL Data
 *  Dont edit / Dont remove this!
 */
	include("configMysql.php");

	mysql_connect(HOST, USER, PASS);
	mysql_select_db(DB);
	
	function funcinc($file) {
		$file = 'functions/'.$file.'.func.php';
		include($file);
	}
	function classinc($file) {
		$file = 'class/'.$file.'.class.php';
		include($file);
	}
	function libinc($file) {
		include('lib/'.$file);
	}
	/* NEWS FUNCTION */
	funcinc("news");
	/* PAGE FUNCTION */
	funcinc("page");
	/* USER FUNCTION */
	funcinc("user");
	/* BB CODE CLASS */
	classinc("bb");

	classinc('lang');
	
	libinc('system/Safe.class.php');
	libinc('slm/text.class.php');
	libinc('slm/main.class.php');
	$safe = new Safe();
	
/*	@Function getTemplate()
 *  Include Templates
 */
 
	function getTemplate() {
		if(empty($_SESSION["tg_Template"])) {
			include('templates/standart/index.php');
		}
	}

	function template() {
		if(empty($_SESSION["tg_Template"])) {
			return 'standart';
		} else {
			return $_SESSION["tg_Template"];
		}
	}

define('TITEL', 'TG-CMS - Startseite');

define('PFAD', 'templates/'.template().'/');

include_once('smarty/Smarty.class.php');

function error($mes="", $um=0) {
	if($um === 0) {
		return '<div class="serror">'.$mes.'</div>';
	} else {
		return '<div class="serror">'.$mes.'</div>';
	}
}

function rep($w1, $w2, $w3) {
	return str_replace($w1, $w2, $w3);
}

$er = strtolower(urldecode($_SERVER['QUERY_STRING']));
$failed = array("union","select","into","where","update ","from","/*","set ","user ","user(","user`","phpinfo",
	"escapeshellarg","exec","fopen","fwrite","escapeshellcmd","passthru","proc_close","proc_get_status","proc_nice",
	"proc_open","proc_terminate","shell_exec","system","telnet","ssh","cmd","mv","chmod","chdir","locate","killall",
	"passwd","kill","script","bash","perl","mysql","~root",".history","~nobody","getenv","and","AND", "1=0","1=1","version", "(", ")");
$check = str_replace($failed, '*', $er);
if($er != $check) {
	$ip = $_SERVER['REMOTE_ADDR'];
	$time = time();
	$new = mysql_query("SELECT IP FROM tg_error_log WHERE IP = '".$ip."'");
	if(mysql_num_rows($new) == 0) {
		$sql = mysql_query("INSERT INTO tg_error_log (IP, Grund, EDate) VALUES ('".$ip."', 'Versuchung eine SQLi einzuspeißen', '".$time."')");
	}
	close("<b>System Error:</b><br /><dd>XSS / SQLi Schutz Akiviert. Ihre IP wird in der Datenbank gespeichert!</dd><br /><br /><dd>Query: ".$er."</dd>", 0);
}
$ip = $_SERVER['REMOTE_ADDR'];
if(empty($_SESSION["tg_Login"])) {
	if(empty($_SESSION["tg_Login_Hash"])) {
		if(empty($_SESSION["tg_userID"])) {
			if(empty($_SESSION["tg_User"])) {
				$con = mysql_query("DELETE FROM tg_wio WHERE IP = '".$ip."'");
			}
		}
	}
} else {
	$con = mysql_fetch_assoc(mysql_query("SELECT userOUT FROM tg_wio WHERE userID = '".$_SESSION["tg_userID"]."'"));
	if($con["userOUT"] <= time()) {
		$con = mysql_query("DELETE FROM tg_wio WHERE userID = '".$_SESSION["tg_userID"]."'");
	}
}
//if(empty($_SESSION["tg_Login"])) { if(!empty($_COOKIE["tg_Login"])) { $_SESSION["tg_Login"] == $_COOKIE["tg_Login"]; }}
//if(empty($_SESSION["tg_Login_Hash"])) { if(!empty($_COOKIE["tg_Login_Hash"])) { $_SESSION["tg_Login_Hash"] == $_COOKIE["tg_Login_Hash"]; }}
//if(empty($_SESSION["tg_userID"])) { if(!empty($_COOKIE["tg_userID"])) { $_SESSION["tg_userID"] == $_COOKIE["tg_userID"]; }}
//if(empty($_SESSION["tg_User"])) { if(!empty($_COOKIE["tg_User"])) { $_SESSION["tg_User"] == $_COOKIE["tg_User"]; }}

?>