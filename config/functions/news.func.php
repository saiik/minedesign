<?php
/*******************************************
 *#########################################*
 *##|_   _|   __|___|     |     |   __|####*
 *####| | |  |  |___|   --| | | |__   |####*
 *####|_| |_____|   |_____|_|_|_|_____|####*
 *#########################################*
 *  (C) by TG-Network and sLy(Tobias F.)   *
 * 		    This is a Free CMS             *
 *  Dont remove this.      (C) 2009-2010   *
 *******************************************/
 
/* @ FUNCTION CONTENT
 * USED VAR. FILE 
 * CONTENT INCLUDE
 * SAFETY AND FAST
 */
	function content($file="") {
		if(empty($file)) {
			include("plugins/news.php");
		} else {
			$array = array("configMain", "configMysql", "mysql", "config");
			if(!in_array($file, $array)) {
				$file = 'plugins/'.$file.'.php';
				if(file_exists($file)) {
					include($file);
				} else {
					close('Diese Datei <b>'.$file.'</b> wurde nicht gefunden!', 0);
				}
			} else {
				close('Das laden der <b>Config</b> Dateien, ist verboten!', 0);
			}
		}
	}
	
?>