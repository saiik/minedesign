<?php
/*******************************************
 *#########################################*
 *##|_   _|   __|___|     |     |   __|####*
 *####| | |  |  |___|   --| | | |__   |####*
 *####|_| |_____|   |_____|_|_|_|_____|####*
 *#########################################*
 *  (C) by TG-Network and sLy(Tobias F.)   *
 * 		    This is a Free CMS             *
 *  Dont remove this.      (C) 2009-2010   *
 *******************************************/
	/* @ FUNCTION CLOSE
	 * USED VAR. CLOSE 
	 * PAGE CLOSE
	 * SAFETY AND FAST
	 */
	 
	function close($text="", $on=1) {
		if($on == 0) {
			if(empty($text)) {
				die("Error");
			} else {
				die($text);
			}
		}
	}
	 
	/* @ FUNCTION SHOWNAVI
	 * USED VAR. NONE 
	 * NAVI
	 */
	 
	function shownavi() {
		#$db = new MySQL(HOST, USER, PASS, DB);
		if(!checkLogin()) {
			$con = mysql_query("SELECT * FROM tg_navi WHERE Logged = '0' ORDER BY Sort ASC");
			$smarty = new Smarty();
			$smarty->template_dir = './templates/'.template().'/templates';
			$smarty->compile_dir = './templates/'.template().'/templates_c';
			while($ds = mysql_fetch_assoc($con)) {
				$smarty->assign('link', $ds["Link"]);
				$smarty->assign('name', $ds["Name"]);
				$smarty->display('navi.tpl');
			}
		} else {
			$con = mysql_query("SELECT * FROM tg_navi ORDER BY Sort ASC");
			$smarty = new Smarty();
			$smarty->template_dir = './templates/'.template().'/templates';
			$smarty->compile_dir = './templates/'.template().'/templates_c';
			while($ds = mysql_fetch_assoc($con)) {
				$smarty->assign('link', $ds["Link"]);
				$smarty->assign('name', $ds["Name"]);
				$smarty->display('navi.tpl');
			}
		}
	}
	 
	 
	function moveto($go="", $time=5, $mes="") {
		if(!empty($go)) {
			echo $mes.'<meta http-equiv="refresh" content="'.$time.'; URL='.$go.'">';
		}
	}
	
	function back() {
		$mes = '<br /><br /><a href="javascript:history.back()" class="back">Zur�ck</a>';
		return $mes;
	}

	function SecQuestion($_3="") {
		$_1 = array(1 => "1 + 5 =", 2 => "5 + 15 =", 3 => "15 - 9 =", 4 => "20 + 20 =", 5 => "10 + 1 =");
		$_2 = array(1 => "6", 2 => "20", 3 => "6", 4 => "40", 5 => "11");
		
		$_zufall = rand(1, count($_1));
		if(empty($_3) || $_3 == "") {
			return $_1[$_zufall];
		} else {
			if($_3 == $_2[$_zufall]) {
				return true;
			} else {
				return false;
			}
		}
	}

	function SessionKey($le) {
		$new = 1;
		for($i=0; $i<$le; $i++) {
			switch(rand(1,4)) {
				case 1:
					$new .= rand(48,57);
				break;
				case 2:
					$new .= rand(64,79);
				break;
				case 3:
					$new .= rand(79,90);
				break;
				case 4:
					$new .= rand(94, 120);
				break;
			}
		}
		if(empty($_SESSION["tgl_ses_hash"])) {
			$_SESSION["tgl_ses_hash"] = $new;
			return $new;
		} else {
			return $_SESSION["tgl_ses_hash"];
		}
	}
	
	function getMode($mode) {
		if(empty($_GET[$mode])) {
			return '';
		} else {
			return $_GET[$mode];
		}
	}
	
	function br($mo="") {
		if(!empty($mo)) {
			for($i=0; $i<$mo; $i++) {
				echo '<br />';
			}
		} else {
			echo '<br />';
		}
	}
	
?>