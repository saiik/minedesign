<?php
/*******************************************
 *#########################################*
 *##|_   _|   __|___|     |     |   __|####*
 *####| | |  |  |___|   --| | | |__   |####*
 *####|_| |_____|   |_____|_|_|_|_____|####*
 *#########################################*
 *  (C) by TG-Network and sLy(Tobias F.)   *
 * 		    This is a Free CMS             *
 *  Dont remove this.      (C) 2009-2010   *
 *******************************************/
 
function checkLogin() {
	if(!empty($_SESSION["tg_Login"])) {
		if(!empty($_SESSION["tg_Login_Hash"])) {
			if(!empty($_SESSION["tg_userID"])) {
				if(!empty($_SESSION["tg_User"])) {
					return true;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	} else {
		return false;
	}
}


function checkAdmin() {
	$con = mysql_query("SELECT * FROM tg_admin_group WHERE userID = '".$_SESSION["tg_userID"]."'");
	if(mysql_num_rows($con)) {
		return true;
	} else {
		return false;
	}
}

function checkOnline($user) {
	if(!empty($user)) {
		$con = mysql_query("SELECT * FROM tg_wio WHERE userID = '".$user."'");
		if(mysql_num_rows($con)) {
			$mes = '<img src="templates/standart/images/ballGreenS.png" alt="" title="User is Online" />';
		} else {
			$mes = '<img src="templates/standart/images/ballDisabledS.png" alt="" title="User is Offline" />';
		}

		return $mes;
	}
}

function getuserID($user) {
	$con = mysql_fetch_assoc(mysql_query("SELECT userID FROM tg_user WHERE Username = '".$user."'"));
	return $con["userID"];
}

function getusername($user) {
	$con = mysql_fetch_assoc(mysql_query("SELECT Username FROM tg_user WHERE userID = '".$user."'")); 
	return $con["Username"];
}

function getadminname($user, $group) {
	if(!empty($group)) {
		if(mysql_num_rows(mysql_query("SELECT GAdmin FROM tg_u_group_mem WHERE userID = '".$user."' AND groupID = '".$group."'")) == 1) {
			$name = '<b>'.getusername($user).'</b>';
			return $name;
		} else {
			$name = getusername($user);
			return $name;
		}
	}
}

function logout() {
	unset($_SESSION["tg_Login"]);
	unset($_SESSION["tg_Login_Hash"]);
	unset($_SESSION["tg_userID"]);
	unset($_SESSION["tg_User"]);
	
	moveto('index.php', 3, 'Du hast dich ausgeloggt');
}

?>