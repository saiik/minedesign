<?php
/*******************************************
 *#########################################*
 *##|_   _|   __|___|     |     |   __|####*
 *####| | |  |  |___|   --| | | |__   |####*
 *####|_| |_____|   |_____|_|_|_|_____|####*
 *#########################################*
 *  (C) by TG-Network and sLy(Tobias F.)   *
 * 		    This is a Free CMS             *
 *  Dont remove this.      (C) 2009-2010   *
 *******************************************/
 
	include("../config/configMysql.php");
	$con = mysql_connect(HOST, USER, PASS);
	if($con) {
		mysql_select_db(DB);
		
		mysql_query("CREATE TABLE tg_navi (
			ID int(11) NOT NULL auto_increment PRIMARY KEY,
			Link TEXT NOT NULL,
			Name TEXT NOT NULL,
			Sort int(14) NOT NULL default '0',
			naviID int(14) NOT NULL,
			 Logged int(2) not null default '0'
		)");
		
		mysql_query("CREATE TABLE tg_news (
			ID int(11) NOT NULL auto_increment PRIMARY KEY,
			Titel TEXT NOT NULL,
			NText TEXT NOT NULL,
			Datum int(14) NOT NULL default '0',
			User TEXT NOT NULL,
			Q1 TEXT NOT NULL,
			Q2 TEXT NOT NULL,
			Q3 TEXT NOT NULL,
			Q1T TEXT NOT NULL,
			Q2T TEXT NOT NULL,
			Q3T TEXT NOT NULL,
			NewsID int(14) NOT NULL
		)");
	
		
		mysql_query("CREATE TABLE tg_user (
			userID int(11) NOT NULL auto_increment PRIMARY KEY,
			Username varchar(250) NOT NULL,
			Passwort varchar(250) NOT NULL,
			RDatum int(14) NOT NULL default '0',
			LLogin int(14) NOT NULL default '0',
			Name TEXT NOT NULL,
			VName TEXT NOT NULL,
			ICQ TEXT NOT NULL,
			MSN TEXT NOT NULL,
			AIM TEXT NOT NULL,
			Steam TEXT NOT NULL,
			ESL TEXT NOT NULL,
			Age TEXT NOT NULL,
			Homep TEXT NOT NULL,
			Town TEXT NOT NULL,
			Bild TEXT NOT NULL,
			Email TEXT NOT NULL,
			About TEXT NOT NULL,
			Activ TEXT NOT NULL
		)");
		
		mysql_query("CREATE TABLE tg_banner (
			ID int(11) NOT NULL auto_increment PRIMARY KEY,
			Link VARCHAR(255) NOT NULL,
			Name VARCHAR(255) NOT NULL,
			Banner VARCHAR(255) NOT NULL
		)");
		
		mysql_query("CREATE TABLE tg_admin_group (
			ID int(11) NOT NULL auto_increment PRIMARY KEY,
			SAdmin VARCHAR(255) NOT NULL default '0',
			NAdmin VARCHAR(255) NOT NULL default '0',
			UAdmin VARCHAR(255) NOT NULL default '0',
			GAdmin VARCHAR(255) NOT NULL default '0',
			userID int(14) NOT NULL
		)");

		mysql_query("CREATE TABLE tg_news_cat (
				ID int(11) NOT NULL auto_increment PRIMARY KEY,
				NCat VARCHAR(255) NOT NULL default '0',
				NPic VARCHAR(255) NOT NULL default '0'
			)");
			
		mysql_query("CREATE TABLE tg_w_cat (
				ID int(11) NOT NULL auto_increment PRIMARY KEY,
				Cat VARCHAR(255) NOT NULL default '0'
			)");
			
		mysql_query("CREATE TABLE tg_w_cat_u (
				ID int(11) NOT NULL auto_increment PRIMARY KEY,
				Cat VARCHAR(255) NOT NULL default '0',
				UCat VARCHAR(255) NOT NULL default '0'
			)");

		mysql_query("CREATE TABLE tg_w_info (
				ID int(11) NOT NULL auto_increment PRIMARY KEY,
				Cat VARCHAR(255) NOT NULL default '0',
				Titel VARCHAR(255) NOT NULL,
				Uploader VARCHAR(255) NOT NULL,
				Size VARCHAR(255) NOT NULL,
				UDate int(14) NOT NULL default '0',
				Info TEXT NOT NULL,
				Passwort VARCHAR(255) NOT NULL,
				FLinks TEXT NOT NULL,
				Activ int(2) NOT NULL default '0'
			)");

		mysql_query("CREATE TABLE tg_online (
				ID int(11) NOT NULL auto_increment PRIMARY KEY,
				userID int(14) NOT NULL default '0',
				User VARCHAR(255) NOT NULL,
				TimeNow int(14) NOT NULL default '0',
				TimtOut int(14) NOT NULL default '0'
			)");
		mysql_query("CREATE TABLE tg_u_mes (
				ID int(11) NOT NULL auto_increment PRIMARY KEY,
				userID int(14) NOT NULL default '0',
				User VARCHAR(255) NOT NULL,
				Datum int(14) NOT NULL default '0',
				Readed int(14) NOT NULL default '0',
				Message TEXT NOT NULL,
				Betreff TEXT NOT NULL
			)");
			
		mysql_query("CREATE TABLE tg_news_com (
				comID int(11) NOT NULL auto_increment PRIMARY KEY,
				newsID int(14) NOT NULL default '0',
				CDate int(14) NOT NULL default '0',
				CText TEXT NOT NULL,
				CIP int(14) NOT NULL default '0',
				userID int(14) NOT NULL default '0'
			)");

		mysql_query("CREATE TABLE tg_u_group (
				groupID int(11) NOT NULL auto_increment PRIMARY KEY,
				GName VARCHAR(255) NOT NULL,
				GJoin int(14) NOT NULL default '0',
				GInfo TEXT NOT NULL,
				Pub int(4) NOT NULL default '1',
				Passwort VARCHAR(255) NOT NULL,
				GGR VARCHAR(255) NOT NULL
			)");
		
		mysql_query("CREATE TABLE tg_about (
				AText TEXT NOT NULL,
				ADate int(14) NOT NULL default '0'
			)");
		
		mysql_query("CREATE TABLE tg_u_mes_out (
				ID int(11) NOT NULL auto_increment PRIMARY KEY,
				userID int(14) NOT NULL default '0',
				User VARCHAR(255) NOT NULL,
				Datum int(14) NOT NULL default '0',
				Readed int(14) NOT NULL default '0',
				Message TEXT NOT NULL,
				Betreff TEXT NOT NULL
			)");
		
		mysql_query("CREATE TABLE tg_wio (
				ID int(11) NOT NULL auto_increment PRIMARY KEY,
				userID int(14) NOT NULL default '0',
				userIP int(14) NOT NULL default '0',
				userTIME int(14) NOT NULL default '0',
				userOUT int(14) NOT NULL default '0'
			)");
		
		mysql_query("CREATE TABLE tg_news_com (
				comID int(11) NOT NULL auto_increment PRIMARY KEY,
				newsID int(14) NOT NULL default '0',
				CDate int(14) NOT NULL default '0',
				CText TEXT NOT NULL,
				CIP int(14) NOT NULL default '0',
				userID int(14) NOT NULL default '0'
			)");
		
		mysql_query("CREATE TABLE tg_u_group_mem (
				ID int(11) NOT NULL auto_increment PRIMARY KEY,
				groupID int(14) NOT NULL default '0',
				userID int(14) NOT NULL default '0',
				DJoin int(14) NOT NULL default '0',
				GAdmin int(2) NOT NULL default '0',
				GMod int(2) NOT NULL default '0'
			)");
		
		mysql_query("CREATE TABLE tg_error_log (
				ID int(11) NOT NULL auto_increment PRIMARY KEY,
				IP VARCHAR(255) NOT NULL default '0',
				Grund TEXT NOT NULL,
				EDate int(14) NOT NULL default '0'
			)");
	}
?>