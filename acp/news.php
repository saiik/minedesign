<?php
/*******************************************
 *#########################################*
 *##|_   _|   __|___|     |     |   __|####*
 *####| | |  |  |___|   --| | | |__   |####*
 *####|_| |_____|   |_____|_|_|_|_____|####*
 *#########################################*
 *  (C) by TG-Network and sLy(Tobias F.)   *
 * 		    This is a Free CMS             *
 *  Dont remove this.      (C) 2009-2010   *
 *******************************************/

	chdir('../');
	include_once('config/configMain.php');
	chdir('acp');
 
	if(!checkLogin()) close(moveto('../index.php', 3, 'Acces Denid').' ', 0);
	if(!checkAdmin()) close(moveto('../index.php', 3, 'Acces Denid').' ', 0);
	
	$mode = getMode('mode');
	
	$smarty = new Smarty();
	$smarty->template_dir = './templates';
	$smarty->compile_dir = './templates_c';
	
	if(empty($mode)) {
		echo '<h3>News Schreiben:</h3>';
		if(empty($_POST["n_save"])) {
			$con = mysql_query("SELETT * FROM tg_news_cat");
			if($con) {
				$smarty->assign('error', '');
				$smarty->assign('dive', '');
				$smarty->assign('dive2', '');
				$smarty->display("news_add.tpl");
			} else {
				$smarty->assign('error', '');
				$smarty->assign('dive', '');
				$smarty->assign('dive2', '');
				$smarty->assign('cat', '');
				$smarty->display("news_add.tpl");
			}
		} else {
		
			$error = array();
		
			if(empty($_POST["titel"])) {
				$error[] = error('Bitte gebe ein Titel ein', 1);
			} else {
				$titel = $_POST["titel"];
			}
			if(empty($_POST["message"])) {
				$error[] = error('Bitte gebe ein News ein', 0);
			} else {
				$news = $_POST["message"];
			}
			
			if(!empty($error)) {
				$smarty->assign('dive', '<div class="error">');
				$smarty->assign('dive2', '</div>');
				$smarty->assign('error', $error);
				$smarty->display("news_add.tpl");
			} else {
				$con = mysql_query("INSERT INTO tg_news (Titel, NText, User, Datum) VALUES ('".$titel."', '".$news."', '".$_SESSION["tg_User"]."', '".time()."')");
				if($con) {
					moveto('index.php?site=news', 3, 'News wurde hinzugef�gt');
				} else {
					moveto('index.php?site=news', 3, 'MySQL Error '.mysql_error());
				}
			}
		}
	} elseif($mode == 'edit') {
		echo '<h3>News Editieren:</h3>';
		if(empty($_GET["id"])) {
			$con = mysql_query("SELECT * FROM tg_news ORDER BY ID DESC");
			if(mysql_num_rows($con)) {
				while($ds = mysql_fetch_assoc($con)) {
					$smarty->assign('titel', $ds["Titel"]);
					$smarty->assign('id', $ds["ID"]);
					$smarty->assign('user', $ds["User"]);
					$smarty->display("news_show.tpl");
				}
			} else {
				echo 'Keine News vorhanden';
			}
		} else {
			if(empty($_POST["s_news"])) {
				$id = $_GET["id"];
				$con = mysql_query("SELECT * FROM tg_news WHERE ID = '".$id."'");
				$ds = mysql_fetch_assoc($con);
				$smarty->assign('titel', $ds["Titel"]);
				$smarty->assign('id', $ds["ID"]);
				$smarty->assign('NText', $ds["NText"]);
				$smarty->display("news_edit.tpl");
			} else {
				$error = array();
			
				if(empty($_POST["titel"])) {
					$error[] = error('Bitte gebe ein Titel ein', 1);
				} else {
					$titel = $_POST["titel"];
				}
				if(empty($_POST["message"])) {
					$error[] = error('Bitte gebe ein News ein', 0);
				} else {
					$news = $_POST["message"];
				}
				
				if(!empty($error)) {
					$smarty->assign('dive', '<div class="error">');
					$smarty->assign('dive2', '</div>');
					$smarty->assign('error', $error);
					$smarty->display("news_edit.tpl");
				} else {
					$con = mysql_query("UPDATE tg_news SET Titel = '".$titel."', NText = '".$news."' WHERE ID = '".$_POST["id"]."'");
					if($con) {
						moveto('index.php', 3, 'News wurde Bearbeitet');
					} else {
						die('MySQL Error:<br />'.mysql_error());
					}
				}
			}
		}
	} elseif($mode == 'del') {
		echo '<h3>News L�schen:</h3>';
		if(empty($_GET["id"])) {
			$con = mysql_query("SELECT * FROM tg_news ORDER BY ID DESC");
			if(mysql_num_rows($con)) {
				while($ds = mysql_fetch_assoc($con)) {
					$smarty->assign('titel', $ds["Titel"]);
					$smarty->assign('id', $ds["ID"]);
					$smarty->assign('user', $ds["User"]);
					$smarty->display("news_show_d.tpl");
				}
			} else {
				echo 'Keine News vorhanden';
			}
		} else {
			$id = $_GET["id"];
			$con = mysql_query("DELETE FROM tg_news WHERE ID = '".$id."'");
			if($con) {
				moveto('index.php', 3, 'News wurde gel�scht!');
			} else {
				die('MySQL Error:<br />'.mysql_error());
			}
		}
	} elseif($mode == 'cat') {
		echo '<h4>News Kategories:</h4>';
		if(empty($_GET["action"])) {
			$action = '';
		} else {
			$action = $_GET["action"];
		}
		echo '<form action="index.php?site=news&mode=cat&action=add" method="post">
		<input type="submit" value="News Kategorie hinzuf�gen" />
		</form><hr size="1" /><br />';
		if(empty($action)) {
			$con = mysql_query("SELECT * FROM tg_news_cat");
			if(mysql_num_rows($con)) {
				while($ds = mysql_fetch_assoc($con)) {
					$smarty->assign('cat', $ds["NCat"]);
					$smarty->assign('id', $ds["ID"]);
					$smarty->display('n_cat_show.tpl');
				}
			} else {
				echo 'Keine News Kategorien vorhanden';
			}
		} elseif($action == 'add') {
			if(empty($_POST["s_cat"])) {
				$smarty->assign('error', '');
				$smarty->display("n_cat_add.tpl");
			} else {
				if(empty($_POST["cat"])) {
					$error = error('Bitte gebe eine News Kategorie an<br />', 1);
				} else {
					$cat = $_POST["cat"];
				}
				
				if(!empty($error)) {
					$smarty->assign('error', $error);
					$smarty->display("n_cat_add.tpl");	
				} else {
					$con = mysql_query("INSERT INTO tg_news_cat (NCat) VALUES ('".$cat."')");
					if($con) {
						moveto('index.php', 3, 'News Kategorie wurde hinzugef�gt');
					} else {
						die('MySQL Error: <br /># '.mysql_error().'<br /># Query: '.$con);
					}
				}
			}
		} elseif($action == 'del') {
			if(!empty($_GET["id"])) {
				$id = $_GET["id"];
				$con = mysql_query("DELETE FROM tg_news_cat WHERE ID = '".$id."'");
				if($con) {
					moveto('index.php', 3, 'News Kategorie wurde gel�scht');
				} else {
					die('MySQL Error: <br /># '.mysql_error().'<br /># Query: '.$con);
				}
			} else {
				close('', 0);
			}
		}	
	}
?>