<?php
/*******************************************
 *#########################################*
 *##|_   _|   __|___|     |     |   __|####*
 *####| | |  |  |___|   --| | | |__   |####*
 *####|_| |_____|   |_____|_|_|_|_____|####*
 *#########################################*
 *  (C) by TG-Network and sLy(Tobias F.)   *
 * 		    This is a Free CMS             *
 *  Dont remove this.      (C) 2009-2010   *
 *******************************************/
	chdir('../');
	include('config/configMain.php');
	chdir('acp');
	if(!checkLogin()) close(moveto('../index.php', 3, 'Acces Denid').' ', 0);
	if(!checkAdmin()) close(moveto('../index.php', 3, 'Acces Denid').' ', 0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
	
	<link rel="stylesheet" type="text/css" href="style.css">
	
	<script type="text/javascript" src="../templates/standart/js/tg.js"></script>
	
	<title>TG-CMS -- Admin-Center</title>
</head>
<body>
<div align="center">
	<div id="Wrapper">
		<span id="Header">
			TG-CMS - Admin-Center
		</span><br /><br />
		<div id="Navi">
			<a href="index.php">Einstellungen</a><br /><hr size="1" />
			<a href="index.php?site=news">News</a><br /><hr size="1" />
			<a href="index.php?site=user">User</a><br /><hr size="1" />
			<a href="index.php">Downloads</a><br /><hr size="1" />
		</div>
		<div id="Con">
			<div id="Top">
				<?php
					if(!empty($_GET["site"])) {
						if($_GET["site"] == "news") {
							echo '<a href="index.php?site=news">News Schreiben</a> - <a href="index.php?site=news&mode=edit">News Editieren</a> - <a href="index.php?site=news&mode=del">News L�schen</a>';
						} elseif($_GET["site"] == "user") {
							echo '<a href="index.php?site=user">Alle User</a> - <a href="index.php?site=user">User Erstellen</a> - <a href="index.php?site=user">User L�schen</a>';
						}
					}
				?>
			</div>
			<div id="News">
				<?php
				if(isset($_GET["site"])) {
					include($_GET["site"].'.php');
				} else {
					echo 'Willkommen im Admin-Center';
				}
				?>
			</div>
		</div>
		<br style="clear: both" />
	</div>
</div>
</body>
</html>