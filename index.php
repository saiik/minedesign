<?php
/*******************************************
 *#########################################*
 *##|_   _|   __|___|     |     |   __|####*
 *####| | |  |  |___|   --| | | |__   |####*
 *####|_| |_____|   |_____|_|_|_|_____|####*
 *#########################################*
 *  (C) by TG-Network and sLy(Tobias F.)   *
 * 		    This is a Free CMS             *
 *  Dont remove this.      (C) 2009-2010   *
 *******************************************/

/*************************************************************** 
 * DIESE DATEI DIENT NUR ZUM INCLUDE                           *
 * DER VERSCHIEDENEN DATEIEN                                   *
 * BITTE L�SCHE DIESE DATEI NICHT                              *
 * DA ES SONST ZU EIN FEHLER KOMMT                             *
 * UND DIESE SEITE NICHT MEHR FUNKTIONIERT                     *
 * DONT EDIT ABOVED THIS LINE                                  *
 ***************************************************************/
 
 
/* INCLUDE CONFIG FILE
 * DONT REMOVE IT
 */
/********************************/
include('config/configMain.php');
/********************************/

/****************************/ #LOAD TEMPLATE FILES
/******/getTemplate();/******/ #ONLY INCLUDE THE INDEX OF THE TEMPLATE
/****************************/ #DONT REMOVE THIS!
/****************************/


/*
	END
		OF FILE
*/
?>