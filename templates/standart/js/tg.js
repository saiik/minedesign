	var check = false;
	
	function spoiler() {
		if(check == false) {
			document.getElementsById('spoiler').style.display = 'none';
			check = true;
		} else {
			document.getElementsById('spoiler').style.display = 'block';
			check = false;
		}
	}
	
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 */
var form = 'post';
var textarea = 'message';
var which=null;

function AddTag(open, close, content) {
	if(typeof(document.forms[form].elements[textarea])=='undefined'){
		if(which==null){
			var textfield = document.forms[form].elements['message[0]'];
		}
		else{
			var textfield = which;
		}
	}
	else{
		var textfield = document.forms[form].elements[textarea];
	}
	textfield.focus();
	if(typeof document.selection != 'undefined') {
		var range = document.selection.createRange();
		if(content == ''){
			var toinsert = range.text;
		}
		else{
			toinsert = content;
		}
		range.text = open + toinsert + close;
		range = document.selection.createRange();
		if (toinsert.length == 0) {
			range.move('character', -close.length);
		}
		else {
			range.moveStart('character', open.length + toinsert.length + close.length);      
		}
		range.select();
	}
	else if(typeof textfield.selectionStart != 'undefined') {
		var start = textfield.selectionStart;
		var end = textfield.selectionEnd;
		if(content == ''){
			var toinsert = textfield.value.substring(start, end);
		}
		else{
			toinsert = content;
		}
		textfield.value = textfield.value.substr(0, start) + open + toinsert + close + textfield.value.substr(end);
		var pos;
		if (toinsert.length == 0) {
			pos = start + open.length;
		} 
		else {
			pos = start + open.length + toinsert.length + close.length;
		}
		textfield.selectionStart = pos;
		textfield.selectionEnd = pos;
	}
	else {
		if(content == ''){
			var toinsert=open + close;
		}
		else{
			toinsert=open + content + close;
		}
		textfield.innerHTML+=toinsert;
	}
}

// insert [img] tag
function AddImg() {
	AddTag('[img]', '[/img]', '');
}

// insert [b] tag
function AddB() {
	AddTag('[b]', '[/b]', '');
}

// insert [U] tag
function AddU() {
	AddTag('[u]', '[/u]', '');
}

// insert [I] tag
function AddI() {
	AddTag('[i]', '[/i]', '');
}

function AddCenter() {
	AddTag('[center]', '[/center]', '');
}

// insert [U] tag
function AddLeft() {
	AddTag('[left]', '[/left]', '');
}

// insert [I] tag
function AddRight() {
	AddTag('[right]', '[/right]', '');
}

// insert [I] tag
function AddSize10() {
	AddTag('[size=10]', '[/size]', '');
}

function AddSize15() {
	AddTag('[size=15]', '[/size]', '');
}

function AddSize20() {
	AddTag('[size=20]', '[/size]', '');
}

function AddSize25() {
	AddTag('[size=25]', '[/size]', '');
}

function AddSize30() {
	AddTag('[size=30]', '[/size]', '');
}

function AddSize35() {
	AddTag('[size=35]', '[/size]', '');
}

function AddColorRed() {
	AddTag('[color=red]', '[/color]', '');
}

function AddColorBlau() {
	AddTag('[color=blue]', '[/color]', '');
}

function AddColorGreen() {
	AddTag('[color=green]', '[/color]', '');
}


function AddArial() {
	AddTag('[font=arial]', '[/font]', '');
}

function AddVerdan() {
	AddTag('[font=Verdana]', '[/font]', '');
}

function AddTimes() {
	AddTag('[font=Times new Roman]', '[/font]', '');
}

// insert [quote] tag
function AddQuote() {
	AddTag('[quote]', '[/quote]', '');
}

