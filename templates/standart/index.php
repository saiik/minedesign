<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
	
	<meta name="Session_Key" content="<?php echo SessionKey(15); ?>" />
	<link rel="stylesheet" type="text/css" href="templates/standart/css/design.css">
	<link rel="stylesheet" type="text/css" href="templates/standart/css/main.css">
	<link rel="stylesheet" type="text/css" href="templates/standart/css/class.css">
	<link rel="stylesheet" type="text/css" href="templates/standart/css/bb.css">
	<script type="text/javascript" src="templates/standart/js/tg.js"></script>
	<base href="http://sly-media.de/mine/" />
	<title><?php echo TITEL; ?></title>
</head>
<body>

<div align="center">
	<div id="Wrapper">
	<br /><br /><span id="logo">MINE<font color="#ec6428"><i>DESIGN</i></font></span><br /><br /><br />
		
		<div id="Left">
			<div id="Warez">
				<h4>Texturepacks</h4>
				<?php include("plugins/wcat_s.php"); ?>
			</div>
			<br />
			<div id="User">
				<h4>User-Center</h4>
				<?php include('plugins/login_s.php'); ?>
			</div>
		</div>
		<div id="Content">
			<div id="Navi">
				<ul>	
					<?php echo shownavi(); ?>
				</ul>
			</div>

			<div id="News">
				<div style="padding-left: 15px;">
				<?php
					if(empty($_GET["content"])) {
						content("news");
					} else {
						$fail = array("/", "\\", "//\\", " ", ".", ",", "Mysql", "config");
						$new = str_replace($fail, '_', $_GET["content"]);
						content($new);
						unset($_GET["content"]);
					}
				?>
				</div>
			</div>
			<br style="clear: both;">
		</div>
		<br />
			<div id="Unten">
				<div style="font-size: 11px; font-family: arial;">
					&copy; by sLy-Media 2011.<br />
					All rights reserved 2009-2011.<br /><br />
					Texturen von HoneyBallLP / PusteKuchen &copy; 2011.
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>