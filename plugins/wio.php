<?php
/*******************************************
 *#########################################*
 *##|_   _|   __|___|     |     |   __|####*
 *####| | |  |  |___|   --| | | |__   |####*
 *####|_| |_____|   |_____|_|_|_|_____|####*
 *#########################################*
 *  (C) by TG-Network and sLy(Tobias F.)   *
 * 		    This is a Free CMS             *
 *  Dont remove this.      (C) 2009-2010   *
 *******************************************/
 if(checkLogin()) {
 
	$smarty = new Smarty();
	$smarty->template_dir = './templates/'.template().'/templates';
	$smarty->compile_dir = './templates/'.template().'/templates_c';
	$bb = new BBCode(1);
	
	echo '<h4>Wer ist Online:</h4>';
	
	$con = mysql_query("SELECT * FROM tg_wio");
	
	if(mysql_num_rows($con)) {
		while($ds = mysql_fetch_assoc($con)) {
			$smarty->assign('user', getusername($ds["userID"]));
			$smarty->assign('on', checkOnline($ds["userID"]));
			$smarty->display('wio.tpl');
		}
	} else {
		echo 'Es ist keiner Online';
	}
	
	echo '<br /><br /><font size="1">Beta!</font>';
 }
?>