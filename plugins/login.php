<?php
/*******************************************
 *	 _____ _____     _____ _____ _____     *
 *	|_   _|   __|___|     |     |   __|    *
 *	  | | |  |  |___|   --| | | |__   |    *
 *	  |_| |_____|   |_____|_|_|_|_____|    *
 *                                         *
 *  (C) by TG-Network and sLy(Tobias F.)   *
 *   Developed by sLy / SinetiX / Saphir   *
 *  Dont remove this.      (C) 2009-2010   *
 *******************************************/
	$smarty = new Smarty();
	$smarty->template_dir = './templates/'.template().'/templates';
	$smarty->compile_dir = './templates/'.template().'/templates_c';
if(empty($_GET["login"])) {
	if(!checkLogin()) {
		if(empty($_POST["submit"])) {
			$smarty->assign('error', '');
			$smarty->assign('dive', '');
			$smarty->assign('dive2', '');
			$smarty->display('login.tpl');
		} else {
			$error = array();
			
			if(empty($_POST["user"])) {
				$error[] = 'Bitte gebe ein Usernamen ein<br />';
			} else {
				$user = $_POST["user"];
				$failed = array("union","select","into","where","update ","from","/*","set ","user ","user(","user`","phpinfo",
					"escapeshellarg","exec","fopen","fwrite","escapeshellcmd","passthru","proc_close","proc_get_status","proc_nice",
					"proc_open","proc_terminate","shell_exec","system","telnet","ssh","cmd","mv","chmod","chdir","locate","killall",
					"passwd","kill","script","bash","perl","mysql","~root",".history","~nobody","getenv","and","AND", "1=0","1=1","version", "(", ")");
				$check = str_replace($failed, '*', $user);
				if($user != $check) {
					$ip = $_SERVER['REMOTE_ADDR'];
					$time = time();
					$new = mysql_query("SELECT IP FROM tg_error_log WHERE IP = '".$ip."'");
					if(mysql_num_rows($new) == 0) {
						$sql = mysql_query("INSERT INTO tg_error_log (IP, Grund, EDate) VALUES ('".$ip."', 'Versuchung eine SQLi einzuspeißen', '".$time."')");
					}
					close("<b>System Error:</b><br /><dd>XSS / SQLi Schutz Akiviert. Ihre IP wird in der Datenbank gespeichert!</dd><br /><br /><dd>Query: ".$user."</dd>", 0);
				}
			}
			if(empty($_POST["pass"])) {
				$error[] = 'Bitte gebe ein Passwort ein';
			} else {
				$pass = $_POST["pass"];
				$failed = array("union","select","into","where","update ","from","/*","set ","user ","user(","user`","phpinfo",
					"escapeshellarg","exec","fopen","fwrite","escapeshellcmd","passthru","proc_close","proc_get_status","proc_nice",
					"proc_open","proc_terminate","shell_exec","system","telnet","ssh","cmd","mv","chmod","chdir","locate","killall",
					"passwd","kill","script","bash","perl","mysql","~root",".history","~nobody","getenv", "1=0","1=1","version");
				$check = str_replace($failed, '*', $pass);
				if($pass != $check) {
					$ip = $_SERVER['REMOTE_ADDR'];
					$time = time();
					$new = mysql_query("SELECT IP FROM tg_error_log WHERE IP = '".$ip."'");
					if(mysql_num_rows($new) == 0) {
						$sql = mysql_query("INSERT INTO tg_error_log (IP, Grund, EDate) VALUES ('".$ip."', 'Versuchung eine SQLi einzuspeißen', '".$time."')");
					}
					close("<b>System Error:</b><br /><dd>XSS / SQLi Schutz Akiviert. Ihre IP wird in der Datenbank gespeichert!</dd><br /><br /><dd>Query: ".$pass."</dd>", 0);
				}
			}
			
			if(!empty($error)) {
				$smarty->assign('dive', '<div class="lerror">');
				$smarty->assign('dive2', '</div>');
				$smarty->assign('error', $error);
				$smarty->display('login.tpl');
			} else {
				$pass_new = '';
				for($i=0; $i<count($pass); $i++) {
					$pass_new = PW_HASH.$pass.PW_HASH;
				}
				$pass_new = md5($pass_new);
				$con = mysql_query("SELECT userID FROM tg_user WHERE Username = '".$user."' AND Passwort = '".$pass_new."'");
				if(mysql_num_rows($con)) {	
					$ds = mysql_fetch_assoc($con);
					$hash = md5(str_shuffle("sdfkjsdfvxksldfjslk.-"));
					$_SESSION["tg_Login"] = 1;
					$_SESSION["tg_Login_Hash"] = $hash;
					$_SESSION["tg_userID"] = $ds["userID"];
					$_SESSION["tg_User"] = $user;
					echo '<div class="success">Sie haben sich Eingeloggt!<br /><a href="index.php">Klicken Sie hier um Weitergeleitet zuwerden</a></div>';
					moveto("index.php", 3);
				} else {
					echo '<div class="lerror">Falsche Benutzerdaten!</div>';
					moveto("index.php?content=login", 3);
				}	
			}
			
		}
	}
} elseif($_GET["login"] == "false") {
	unset($_SESSION["tg_Login"]);
	unset($_SESSION["tg_Login_Hash"]);
	unset($_SESSION["tg_userID"]);
	unset($_SESSION["tg_User"]);
	echo '<div class="success">Sie haben sich Ausgeloggt!<br /><a href="index.php">Klicken Sie hier um Weitergeleitet zuwerden</a></div>';
	moveto("index.php", 3);
}
?>