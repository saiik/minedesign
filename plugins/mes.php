<?php
/*******************************************
 *#########################################*
 *##|_   _|   __|___|     |     |   __|####*
 *####| | |  |  |___|   --| | | |__   |####*
 *####|_| |_____|   |_____|_|_|_|_____|####*
 *#########################################*
 *  (C) by TG-Network and sLy(Tobias F.)   *
 * 		    This is a Free CMS             *
 *  Dont remove this.      (C) 2009-2010   *
 *******************************************/
 if(checkLogin()) {
 
	$smarty = new Smarty();
	$smarty->template_dir = './templates/'.template().'/templates';
	$smarty->compile_dir = './templates/'.template().'/templates_c';
	$bb = new BBCode(1);
	$safe = new Safe();
	
	$mode = getMode('mode');
	echo '<h4>Nachrichten:</h4>';
	if(empty($mode)) {
		/****** ALL MESSAGES */
		echo '<button class="form" onclick="location.href=\'index.php?content=mes&mode=add\'">Neue Nachricht</button> - <button class="form" onclick="location.href=\'index.php?content=mes&mode=out\'">Postausgang</button><br /><br />';
		$con = mysql_query("SELECT * FROM tg_u_mes WHERE userID = '".$_SESSION["tg_userID"]."' ORDER BY ID DESC");
		if(mysql_num_rows($con)) {
			$hash = $safe->generateHash("tg_mes_hashID");
			$smarty->display('mes_all_head.tpl');
			while($ds = mysql_fetch_assoc($con)) {
				if($ds["Readed"] == '0') {
					$smarty->assign('read', '<img src="templates/standart/images/iCubeBlueS.png" alt="" />');
				} elseif($ds["Readed"] == '1') {
					$smarty->assign('read', '<img src="templates/standart/images/iCubeGreyS.png" alt="" />');
				}	
				$smarty->assign('id', $ds["ID"]);
				$smarty->assign('user', getusername($ds["User"]));
				$smarty->assign('bet', $ds["Betreff"]);
				$smarty->assign('hash', $_SESSION["tg_mes_hashID"]);
				$smarty->display('mes_all.tpl');
			}
			echo '</table>';
		} else {
			echo 'Keine Nachrichten vorhanden!';
		}
	} elseif($mode == 'add') {
		if(empty($_POST["s_mes"])) {
			if(!empty($_POST["user"]) && !empty($_POST["betreff"])) {
				$mes = str_replace('Re:', '', $_POST["betreff"]);
				$smarty->assign('user', $_POST["user"]);
				$smarty->assign('bet', 'Re: '.$mes);
			}
			$smarty->display('mes_add.tpl');
			echo '<br /><br /><br /><a href="index.php?content=mes" class="back">Zur�ck</a>';
		} else {
			if(empty($_POST["user"])) {
				$er1 = error('Bitte gebe ein Usernamen an<br />', 1);
			} else {
				$user = $_POST["user"];
			}
			
			if(empty($_POST["bet"])) {
				$er2 = error('Bitte gebe ein Betreff an', 1);
			} else {
				$bet = $_POST["bet"];
			}
			
			if(empty($_POST["message"])) {
				$er3 = error('Bitte gebe ein Message ein', 1);
			} else {
				$mes = $_POST["message"];
			}
			if(!empty($er1) || !empty($er2) || !empty($er3)) {
				$smarty->assign('er1', $er1);
				$smarty->assign('er2', $er2);
				$smarty->assign('er3', $er3);
				$smarty->display('mes_add.tpl');
			} else {
				$userID = getuserID($user);
				$time = time();
				if(mysql_num_rows(mysql_query("SELECT userID FROM tg_user WHERE userID = '".$userID."'"))) {
					if($userID != $_SESSION["tg_userID"]) {
						$con = mysql_query("INSERT INTO tg_u_mes (userID, User, Datum, Message, Betreff) VALUES ('".$userID."', '".$_SESSION["tg_userID"]."', '".$time."', '".$mes."', '".$bet."')");
						$con = mysql_query("INSERT INTO tg_u_mes_out (userID, User, Datum, Message, Betreff) VALUES ('".$userID."', '".$_SESSION["tg_userID"]."', '".$time."', '".$mes."', '".$bet."')");
						if($con) {
							moveto('index.php?content=mes', 3, 'Message wurde verschickt');
						} else {
							moveto('index.php?content=mes', 5, 'Message wurde nicht verschickt!');
						}
					} else {
						moveto('index.php?content=mes', 3, 'Du kannst dir keine Nachrichten schicken');
					}
				} else {
					moveto('index.php?content=mes', 3, 'Kein User mit dem Namem '.$user.' vorhanden');
				}
			}
			echo '<br /><br /><br /><a href="index.php?content=mes" class="back">Zur�ck</a>';
		}
	} elseif($mode == 'read') {
		$id = $_GET["mesID"];
		if(!empty($id)) {
			$con = mysql_query("SELECT * FROM tg_u_mes WHERE ID = '".$id."'");
			if(mysql_num_rows($con)) {
				$ds = mysql_fetch_assoc($con);
				$smarty->assign('user', getusername($ds["User"]));
				$smarty->assign('user_add', getusername($ds["User"]));
				$smarty->assign('bet', $ds["Betreff"]);
				$smarty->assign('mes', $bb->BBParser($ds["Message"]));
				$smarty->assign('date', date("d.m.Y - H:i", $ds["Datum"]));
				if($ds["User"] === '1000') {
					$smarty->display('mes_read2.tpl');
				} else {
					$smarty->display('mes_read.tpl');
				}
				if($ds["Readed"] == 0) {
					$sql = mysql_query("UPDATE tg_u_mes set Readed = '1' WHERE ID = '".$id."'") or die(myqsl_error());
				}
			}
			echo '<br /><br /><br /><a href="index.php?content=mes" class="back">Zur�ck</a>';
		}
	} elseif($mode == 'del') {
		$id = $_GET["mesID"];
		if(!empty($id)) {
			if($_GET["hashID"] == $_SESSION["tg_mes_hashID"]) {
				$con = mysql_query("SELECT ID FROM tg_u_mes WHERE User = '".$_SESSION["tg_userID"]."' AND ID = '".$id."'");
				if(mysql_num_rows($con)) {
					$sql = mysql_query("DELETE FROM tg_u_mes WHERE ID = '".$id."' AND User = '".$_SESSION["tg_userID"]."'");
					if($sql) {
						moveto('index.php?content=mes', 3, 'Nachricht wurde gel�scht');
					} else {
						moveto('index.php?content=mes', 5, 'Nachricht wurde nicht gel�scht');
					}
				} else {
					moveto('index.php?content=mes', 3, 'Sie haben keine Rechte diese Nachricht zul�schen');
				}
			} else {
				moveto('index.php?content=mes', 3, 'HashID Error');
			}
		} else {
			moveto('index.php?content=mes', 3, 'ID Error');
		}
	} elseif($mode == 'out') {
		echo '<button class="form" onclick="location.href=\'index.php?content=mes&mode=add\'">Neue Nachricht</button><br />
		<h4>Postausgang:</h4>';
		$con = mysql_query("SELECT * FROM tg_u_mes_out WHERE User = '".$_SESSION["tg_userID"]."' ORDER BY ID DESC");
		if(mysql_num_rows($con)) {
			$smarty->display('mes_out_head.tpl');
			while($ds = mysql_fetch_assoc($con)) {
				$new = mysql_fetch_assoc(mysql_query("SELECT * FROM tg_u_mes WHERE userID = '".$ds["userID"]."' ORDER BY ID DESC"));
				if($new["Readed"] == '0') {
					$smarty->assign('read', '<img src="templates/standart/images/iCubeBlueS.png" alt="" title="Nachricht wurde noch nicht gelesen!" />');
				} elseif($new["Readed"] == '1') {
					$smarty->assign('read', '<img src="templates/standart/images/iCubeGreyS.png" alt="" title="Nachricht wurde gelesen!" />');
				}	
				$smarty->assign('id', $ds["ID"]);
				$smarty->assign('user', getusername($ds["User"]));
				$smarty->assign('bet', $ds["Betreff"]);
				$smarty->assign('to', getusername($ds["userID"]));
				$smarty->display('mes_out.tpl');
			}
			echo '</table>';
		} else {
			echo 'Keine Nachrichten vorhanden!';
		}
		echo '<br /><br /><br /><a href="index.php?content=mes" class="back">Zur�ck</a>';
	} elseif($mode == 'outread') {
		$id = $_GET["mesID"];
		if(!empty($id)) {
			$con = mysql_query("SELECT * FROM tg_u_mes_out WHERE ID = '".$id."' ORDER BY ID DESC");
			if(mysql_num_rows($con)) {
				$ds = mysql_fetch_assoc($con);
				$smarty->assign('user', getusername($ds["userID"]));
				$smarty->assign('bet', $ds["Betreff"]);
				$smarty->assign('mes', $bb->BBParser($ds["Message"]));
				$smarty->assign('date', date("d.m.Y - H:i", $ds["Datum"]));
				$smarty->display('mes_read_out.tpl');
			}
			echo '<br /><br /><br /><a href="index.php?content=mes&mode=out" class="back">Zur�ck</a>';
		}
	}
	
 }
?>