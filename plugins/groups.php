<?php
/*******************************************
 *#########################################*
 *##|_   _|   __|___|     |     |   __|####*
 *####| | |  |  |___|   --| | | |__   |####*
 *####|_| |_____|   |_____|_|_|_|_____|####*
 *#########################################*
 *  (C) by TG-Network and sLy(Tobias F.)   *
 * 		    This is a Free CMS             *
 *  Dont remove this.      (C) 2009-2010   *
 *******************************************/
 if(checkLogin() === true) {
	// LOAD SMARTY CLASS
	$smarty = new Smarty();
	// SET TEMPLATE DIR
	$smarty->template_dir = './templates/'.template().'/templates';
	// SET CACHE TEMPLATE DIR
	$smarty->compile_dir = './templates/'.template().'/templates_c';
	// LOAD BBCODE CLASS AND SET 1 (ON)
	$bb = new BBCode(1);
	// LOAD SAFE CLASS
	$safe = new Safe();
	// LOAD TEXT CLASS FOR ECHO OUTPUTS
	$text = new Text();
	// H4 TEXT GRUPPEN
	$text->h4("Gruppen:");
	// GET MODE FROM $_GET["MODE"]
	$mode = getMode('mode');
	
	// CHECK MODE
	if(empty($mode)) {
		// OUT BUTTON
		$smarty->assign('to', 'index.php?content=groups&mode=add');
		$smarty->assign('name', 'Gruppe Hinzuf�gen');
		$smarty->display('button.tpl');
		// OUT BUTTON
		$smarty->assign('to', 'group_all.html');
		$smarty->assign('name', 'Alle Gruppen');
		$smarty->display('button.tpl');
		// OUT 2 <BR />
		$text->br(2);
		// SELECT GROUPS FROM GROUP_MEM
		$con = mysql_query("SELECT * FROM tg_u_group_mem WHERE userID = '".$_SESSION["tg_userID"]."'");
		// CHECK IT
		if(mysql_num_rows($con)) {
			echo 'Du bist Mitglied in folgenden Gruppen:<br /><br />';
			// OUT GROUP MY ALL HEAD (HEADER)
			$smarty->display('group_my_all_head.tpl');
			// SET A WHILE FOR THE SQL QUERY
			while($ds = mysql_fetch_assoc($con)) {
				// GET THE GROUP NAME AND THE ID
				$sql = mysql_fetch_assoc(mysql_query("SELECT * FROM tg_u_group WHERE groupID = '".$ds["groupID"]."'"));
				$smarty->assign('titel', $sql["GName"]);
				$smarty->assign('id', $sql["groupID"]);
				$smarty->assign('ak', '<a href="group_leave_'.$ds["groupID"].'.html">Verlassen</a>');
				$smarty->display('group_my_all.tpl');
			}
			// OUT GROUP ALL FOOT (FOOTER)
			$smarty->display('group_all_foot.tpl');
		} else {
			echo 'Du bist kein Mitglied in einer Gruppe';
		}
	} elseif($mode == 'add') {
		if(empty($_POST["g_add"])) {
			$smarty->display('group_add.tpl');
			// A BACK BUTTON
			echo '<br /><br /><a href="group.html" class="back">Zur�ck</a>';
		} else {
			if(empty($_POST["titel"])) {
				$er1 = error('Bitte gebe ein Gruppen Titel an', 1);
			} else {
				$titel = $_POST["titel"];
			}
			if(empty($_POST["mes"])) {
				$er2 = error('Bitte gebe eine Gruppen Information an', 1);
			} else {
				$mes = $_POST["mes"];
			}
			if(empty($_POST["pass"])) {
				$pass = '';
				$pub = '1';
			} else {
				$pass = $_POST["pass"];
				$pub = '0';
			}
			// CHECK FOR ERROR
			if(!empty($er1) || !empty($er2)) {
				$smarty->assign('er1', $er1);
				$smarty->assign('er2', $er2);
				$smarty->display('group_add.tpl');
			} else {
				$time = time();
				$con = mysql_query("INSERT INTO tg_u_group (GName, GJoin, GInfo, Pub, Passwort, GGR) VALUES ('".$titel."', '".$time."', '".$mes."', '".$pub."', '".$pass."', '".$_SESSION["tg_userID"]."')");
				$groupID = mysql_insert_id();
				$sql = mysql_query("INSERT INTO tg_u_group_mem (groupID, userID, DJoin, GAdmin, GMod) VALUES ('".$groupID."', '".$_SESSION["tg_userID"]."', '".time()."', '1', '1')");
				if($con && $sql) {
					echo '<div class="success">Gruppe wurde hinzugef�gt!</div>';
					moveto('index.php?content=groups', 3);
				} else {
					echo '<div class="lerror">Gruppe wurde nicht hinzugef�gt!</div>';
					moveto('index.php?content=groups', 3);
				}
			}
		}
	} elseif($mode == 'show') {
		if(!empty($_GET["groupID"])) {
			$id = $_GET["groupID"];
			// GET THE GROUP DATA WITH A ID
			$con = mysql_query("SELECT * FROM tg_u_group WHERE groupID = '".$id."'");
			$ds = mysql_fetch_assoc($con);
			$admin = mysql_fetch_assoc(mysql_query("SELECT GGR FROM tg_u_group WHERE groupID = '".$id."'"));
			$all = mysql_num_rows(mysql_query("SELECT ID FROM tg_u_group_mem WHERE groupID = '".$id."'"));
			$admin_name = getusername($admin["GGR"]);
			// CHECK FOR ADMIN
			if(empty($admin_name)) {
				$admin_name = 'Keiner (Geleavt)';
			}
			// GET THE JOIN DATE
			$GDate = date('d.m.Y - H:i', $ds["GJoin"]);
			// MAKE BUTTONS FOR LEAVE AND JOIN
			if(mysql_num_rows(mysql_query("SELECT * FROM tg_u_group_mem WHERE groupID = '".$id."' AND userID = '".$_SESSION["tg_userID"]."'"))) {
				$leave = '<form action="index.php?content=groups&mode=leave&groupID='.$ds["groupID"].'" method="post">
				<input type="submit" name="leave" class="form" value="Gruppe Verlassen" />
				</form>';
			} else {
				$leave = '<form action="index.php?content=groups&mode=join&groupID='.$ds["groupID"].'" method="post">
				<input type="hidden" name="groupID" value="'.$ds["groupID"].'" />
				<input type="submit" name="leave" class="form" value="Gruppe Joinen" />
				</form>';
			}
			// CHECK FOR INVITES
			if(mysql_num_rows(mysql_query("SELECT groupID FROM tg_u_group_mem WHERE userID = '".$_SESSION["tg_userID"]."' and groupID = '".$id."'"))) {
				$inv = ' - <a href="index.php?content=groups&mode=invite&groupID='.$ds["groupID"].'" class="FileInfo">Leute einladen</a>';
			} else {
				$inv = '';
			}
			$smarty->assign('titel', $ds["GName"]);
			$smarty->assign('gr', $admin_name);
			$smarty->assign('al', $all);
			$smarty->assign('time', $GDate);
			$smarty->assign('info', $bb->BBParser($ds["GInfo"]));
			$smarty->assign('id', $ds["groupID"]);
			$smarty->assign('inv', $inv);
			$smarty->assign('leave', $leave);
			$smarty->display('group_show.tpl');
			echo '<br /><br /><a href="group.html" class="back">Zur�ck</a>';
		}
	} elseif($mode == 'showall') {
		$smarty->assign('to', 'index.php?content=groups&mode=add');
		$smarty->assign('name', 'Gruppe Hinzuf�gen');
		$smarty->display('button.tpl');
		echo br(2);
		$con = mysql_query("SELECT * FROM tg_u_group");
		if(mysql_num_rows($con)) {
			echo 'Es gibt folgende Gruppen:<br /><br />';
			$smarty->display('group_all_head.tpl');
			$hash = $safe->generateHash("tg_del_g_hashID");
			while($ds = mysql_fetch_assoc($con)) {
				$sql = mysql_fetch_assoc(mysql_query("SELECT GAdmin FROM tg_admin_group WHERE userID = '".$_SESSION["tg_userID"]."'"));
				if($sql["GAdmin"] == 1) {
					$admin = '<a href="index.php?content=groups&mode=del&groupID='.$ds["groupID"].'&hashID='.$_SESSION["tg_del_g_hashID"].'">Gruppe L�schen</a>';
				} else {
					$admin = 'Keine';
				}	
				$smarty->assign('titel', $ds["GName"]);
				$smarty->assign('id', $ds["groupID"]);
				$smarty->assign('admin', $admin);
				$smarty->display('group_all.tpl');
			}
			$smarty->display('group_all_foot.tpl');
		} else {
			echo 'Es gibt keine Gruppen!';
		}
		echo '<br /><br /><a href="group.html" class="back">Zur�ck</a>';
	} elseif($mode == 'showuser') {
		if(!empty($_GET["groupID"])) {
			$id = $_GET["groupID"];
			$text->h4("Alle User:");
			$con = mysql_query("SELECT * FROM tg_u_group_mem WHERE groupID = '".$id."' ORDER BY ID ASC");
			if(mysql_num_rows($con)) {
				$hash = $safe->generateHash("tg_del_g_hashID");
				$smarty->display('group_user_head.tpl');
				while($ds = mysql_fetch_assoc($con)) {
					$sql = mysql_fetch_assoc(mysql_query("SELECT GAdmin, GMod FROM tg_u_group_mem WHERE userID = '".$_SESSION["tg_userID"]."'"));
					if($sql["GAdmin"] == 1 || $sql["GMod"] == 1) {
						$admin = '<a href="group_'.$ds["groupID"].'_hash_'.$_SESSION["tg_del_g_hashID"].'_kick_'.$ds["userID"].'.html">User kicken</a> - <a href="group_'.$ds["groupID"].'_uprank_'.$ds["userID"].'_mod.html">Bef�rden(Mod)</a>';
					} else {
						$admin = 'Keine';
					}
					$anz = mysql_num_rows(mysql_query("SELECT userID FROM tg_u_group_mem WHERE groupID = '".$id."' AND userID = '".$ds["userID"]."' AND GAdmin = '1'"));
					$neu = mysql_num_rows(mysql_query("SELECT userID FROM tg_u_group_mem WHERE groupID = '".$id."' AND userID = '".$ds["userID"]."' AND GMod = '1'"));
					if($anz != 0) {
						$nick = '<font color="red"><b>'.getusername($ds["userID"]).'</b></font>';
					} elseif($anz == 0 && $neu != 0) {
						$nick = '<font color="green"><i>'.getusername($ds["userID"]).'</i></font>';
					} elseif($anz == 0 && $neu == 0) {
						$nick = getusername($ds["userID"]);
					}
					$smarty->assign('info', checkOnline($ds["userID"]));		
					$smarty->assign('user', $nick);
					$smarty->assign('date', date('d.m.Y - H:i', $ds["DJoin"]));
					$smarty->assign('admin', $admin);
					$smarty->display('group_user.tpl');
				}
				$smarty->display('group_all_foot.tpl');
				echo '<font size="1">Legende: <font color="red"><b>Admin</b></font>;<font color="green"><i>Moderator</i></font></font>';
				echo '<br /><br /><a href="group_'.$id.'.html" class="back">Zur�ck</a><br />';
			} else {
				echo 'Keine Member vorhanden';
			}
		}
	} elseif($mode == 'del') {
		if(!empty($_GET["groupID"])) {
			$id = $_GET["groupID"];
			if($_GET["hashID"] == $_SESSION["tg_del_g_hashID"]) {
				if(mysql_num_rows(mysql_query("SELECT groupID FROM tg_u_group WHERE groupID = '".$id."'"))) {
					if(mysql_num_rows(mysql_query("SELECT GAdmin FROM tg_admin_group WHERE userID = '".$_SESSION["tg_userID"]."' AND GAdmin = '1'"))) {
						$sql = mysql_query("DELETE FROM tg_u_group WHERE groupID = '".$id."'");
						$con = mysql_query("DELETE FROM tg_u_group_mem WHERE groupID = '".$id."'");
						if($sql && $con) {
							echo '<div class="success">Diese Gruppe wurde gel�scht!</div>';
							moveto('index.php?content=groups', 3);
						} else {
							echo '<div class="lerror">Das l�schen dieser Gruppe ist fehlgeschlagen</div>';
							moveto('index.php?content=groups', 3);
						}
					} else {
						echo '<div class="lerror">Sie haben keine Rechte diese Gruppe zul�schen</div>';
						moveto('index.php?content=groups', 3);
					}
				} else {
					echo '<div class="lerror">groupID Error</div>';
					moveto('index.php?content=groups', 3);
				}
			} else {
				echo '<div class="lerror">HashID Error</div>';
				moveto('index.php?content=groups', 3);
			}
		} else {
			echo '<div class="lerror">ID Error</div>';
			moveto('index.php?content=groups', 3);
		}
	} elseif($mode == 'kick') {
		if(!empty($_GET["groupID"])) {
			$id = $_GET["groupID"];
			if(!empty($_GET["userID"])) {
				$user = $_GET["userID"];
				if($_GET["hashID"] == $_SESSION["tg_del_g_hashID"]) {
					if(mysql_num_rows(mysql_query("SELECT GAdmin FROM tg_u_group_mem WHERE userID = '".$_SESSION["tg_userID"]."' AND GAdmin = '1' OR GMod = '1'"))) {
						$con = mysql_query("DELETE FROM tg_u_group_mem WHERE userID = '".$user."' AND groupID = '".$id."'");
						$check = mysql_fetch_assoc(mysql_query("SELECT GName FROM tg_u_group WHERE groupID = '".$id."'"));
						$sql = mysql_query("INSERT INTO tg_u_mes (userID, User, Datum, Message, Betreff) VALUES ('".$user."', '1', '".time()."', 'Du wurdest aus der Gruppe:<br /><dd>".$check["GName"]." gekickt!</dd>', 'Gruppen Kick')");
						if($sql && $con) {
							moveto('index.php?content=groups', 3, 'Der User wurde aus der Gruppe gekickt!');
						} else {
							moveto('index.php?content=groups', 3, 'Der User wurde aus der Gruppe nicht gekickt!');
						}
					} else {
						moveto('index.php?content=groups', 3, 'Sie haben keine Rechte diese Gruppe zul�schen');
					}
				} else {
					moveto('index.php?content=groups', 3, 'HashID Error');
				}
			} else {
				moveto('index.php?content=groups', 3, 'userID Error');
			}
		} else {
			moveto('index.php?content=groups', 3, 'ID Error');
		}
	} elseif($mode == 'leave') {
		if(!empty($_GET["groupID"])) {
			$id = $_GET["groupID"];
			if(mysql_query("DELETE FROM tg_u_group_mem WHERE groupID = '".$id."' AND userID = '".$_SESSION["tg_userID"]."'")) {
				moveto('index.php?content=groups', 3, 'Du hast die Gruppe verlassen '.mysql_error());
			} else {
				moveto('index.php?content=groups', 3, 'Du hast die Gruppe nicht verlassen');
			}
		}
	} elseif($mode == 'join') {
		if(!empty($_POST["groupID"]) || !empty($_GET["groupID"])) {
			if(empty($_GET["groupID"])) {
				$id = $_POST["groupID"];
			} else {
				$id = $_GET["groupID"];
			}
			$check = mysql_num_rows(mysql_query("SELECT ID FROM tg_u_group_mem WHERE userID = '".$_SESSION["tg_userID"]."' AND groupID = '".$id."'"));
			if($check == 0) {
			$con = mysql_fetch_assoc(mysql_query("SELECT Pub FROM tg_u_group WHERE groupID = '".$id."'"));
				if($con["Pub"] == 0) {
					if(empty($_POST["g_join"])) {
						$smarty->assign('id', $id);
						$smarty->display('group_join.tpl');
					} else {
						$group = $_POST["groupID"];
						if(!empty($_POST["pass"])) {
							$pass = $_POST["pass"];
							if(mysql_num_rows(mysql_query("SELECT groupID FROM tg_u_group WHERE groupID = '".$group."' AND Passwort = '".$pass."'"))) {
								$sql = mysql_query("INSERT INTO tg_u_group_mem (groupID, userID, DJoin, GAdmin, GMod) VALUES ('".$group."', '".$_SESSION["tg_userID"]."', '".time()."', '0', '0')");
								if($sql) {
									moveto('index.php?content=groups', 3, 'Du bist der Gruppe beigetretten');
								} else {
									moveto('index.php?content=groups', 3, 'Du bist der Gruppe nicht beigetretten');
								}
							}
						}
					}
				} else {
					$sql = mysql_query("INSERT INTO tg_u_group_mem (groupID, userID, DJoin, GAdmin, GMod) VALUES ('".$id."', '".$_SESSION["tg_userID"]."', '".time()."', '0', '0')");
					if($sql) {
						moveto('index.php?content=groups', 3, 'Du bist der Gruppe beigetretten');
					} else {
						moveto('index.php?content=groups', 3, 'Du bist der Gruppe nicht beigetretten');
					}
				}
			} else {
				moveto('index.php?content=groups', 3, 'Du bist schon in der Gruppe!');
			}
		}
	} elseif($mode == 'invite') {
		if(!empty($_GET["groupID"])) {
			$id = $_GET["groupID"];
			if(mysql_num_rows(mysql_query("SELECT GAdmin FROM tg_u_group_mem WHERE userID = '".$_SESSION["tg_userID"]."' AND groupID = '".$id."'"))) {
				if(empty($_POST["g_inv"])) {
					$smarty->assign('id', $id);
					$smarty->display('group_inv.tpl');
				} else {
					if(empty($_POST["user"])) {
						$er = error('Bitte gebe ein Usernamen an', 1);
					} else {
						$userid = getuserID($_POST["user"]);
					}
					if(!empty($er)) {
						$smarty->assign('id', $id);
						$smarty->assign('er', $er);
						$smarty->display('group_inv.tpl');
					} else {
						$check = mysql_fetch_assoc(mysql_query("SELECT GName FROM tg_u_group WHERE groupID = '".$_POST["groupID"]."'"));
						$sql = mysql_query("INSERT INTO tg_u_mes (userID, User, Datum, Message, Betreff) VALUES ('".$userid."', '".$_SESSION["tg_userID"]."', '".time()."', 'Du wurdest in die Gruppe:<br /><dd><a href=\"index.php?content=groups&mode=join&groupID=".$_POST["groupID"]."\" class=\"FileInfo\">".$check["GName"]."</a><br /> Eingeladen!</dd>', 'Gruppen Einladung')");
						if($sql) {
							moveto('index.php?content=groups', 3, 'Einladung wurde verschickt');
						} else {
							moveto('index.php?content=groups', 3, 'Einladung wurde nicht verschickt');
						}
					}
				}
			} else {
				moveto('index.php?content=groups', 3, 'Acces Error');
			}
		} else {
			moveto('index.php?content=groups', 3, 'ID Error');
		}
	}
 } else {
	echo "please login to use this feature..";
 }
?>