<?php
/*******************************************
 *#########################################*
 *##|_   _|   __|___|     |     |   __|####*
 *####| | |  |  |___|   --| | | |__   |####*
 *####|_| |_____|   |_____|_|_|_|_____|####*
 *#########################################*
 *  (C) by TG-Network and sLy(Tobias F.)   *
 * 		    This is a Free CMS             *
 *  Dont remove this.      (C) 2009-2010   *
 *******************************************/
 if(checkLogin() === true) {
	// LOAD SMARTY CLASS
	$smarty = new Smarty();
	// SET TEMPLATE DIR
	$smarty->template_dir = './templates/'.template().'/templates';
	// SET CACHE TEMPLATE DIR
	$smarty->compile_dir = './templates/'.template().'/templates_c';
	// LOAD BBCODE CLASS AND SET 1 (ON)
	$bb = new BBCode(1);
	// LOAD SAFE CLASS
	$safe = new Safe();
	// LOAD TEXT CLASS FOR ECHO OUTPUTS
	$text = new Text();
	// H4 TEXT GRUPPEN
	$text->h4("Guthaben");
	// GET MODE FROM $_GET["MODE"]
	$mode = getMode('mode');
	
	// LADE W�RHUNG
	
	$neu = mysql_fetch_assoc(mysql_query("SELECT Wahrung FROM tg_bank_main"));
	
	$euro = $neu["Wahrung"];
 
	if(empty($mode)) {
		$sql = mysql_fetch_assoc(mysql_query("SELECT Geld FROM tg_bank_user WHERE userID = '".$_SESSION["tg_userID"]."'"));
		$smarty->assign('euro', '<b><u>'.$sql["Geld"].'</u></b> '.$euro);
		$smarty->display('bank_head.tpl');
	} elseif($mode == 'transfer') {
		$text->h4("Guthaben Transfer:");
		if(empty($_POST["submit"])) {
			$smarty->display('bank_tran.tpl');
		} else {
			if(empty($_POST["kn"])) {
				$error_1 = '<div class="serror">Bitte gebe ein Konto-Nummer ein!</div>';
			} else {
				$kn = $_POST["kn"];
			}
			if(empty($_POST["euro"])) {
				$error_2 = '<div class="serror">Bitte gebe ein Betrag ein!</div>';
			} else {
				$euro = $_POST["euro"];
			}
			if(!empty($_POST["info"])) {
				$info = $_POST["info"];
			}
			if(!empty($error_1) || !empty($error_2)) {
				$smarty->assign('er1', $error_1);
				$smarty->assign('er2', $error_2);
				$smarty->display('bank_tran.tpl');
			} else {
			
			}
		}
	}
	
 }
?>