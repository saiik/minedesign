<?php
/*******************************************
 *	 _____ _____     _____ _____ _____     *
 *	|_   _|   __|___|     |     |   __|    *
 *	  | | |  |  |___|   --| | | |__   |    *
 *	  |_| |_____|   |_____|_|_|_|_____|    *
 *                                         *
 *  (C) by TG-Network and sLy(Tobias F.)   *
 *   Developed by sLy / SinetiX / Saphir   *
 *  Dont remove this.      (C) 2009-2010   *
 *******************************************/
	 $text = new Text();
	 $smarty = new Smarty();
	 $smarty->template_dir = './templates/'.template().'/templates';
	 $smarty->compile_dir = './templates/'.template().'/templates_c';
	 
	 $mode = getMode('mode');
	
	 if(empty($mode)) {
		 if(!empty($_GET["cat"])) {
			$id = $_GET["cat"];
			$con = mysql_query("SELECT * FROM tg_w_info WHERE Cat = '".$id."' AND Activ = '1'");
			$da = mysql_fetch_assoc(mysql_query("SELECT * FROM tg_w_cat_u WHERE ID = '".$id."'"));
			$px = str_replace("px", "", $da["Cat"]);
			$text->h4($px."px Texturenpacks:");
			if(checkLogin()) {
				echo '<form action="index.php?content=files&mode=add&cat='.$_GET["cat"].'" method="post">
				<input class="form" type="submit" value="Neues Pack hinzuf�gen" />
				</form><br /><br />';
			}
			if(mysql_num_rows($con)) {
				echo '<div style="border-bottom: 1px #ccc dashed;"></div>';
				while($ds = mysql_fetch_assoc($con)) {
					$smarty->assign('titel', $ds["Titel"]." [".$px."x".$px."]");
					$smarty->assign('up', $ds["Uploader"]);
					$smarty->assign('time', date('d.m.Y - H:i', $ds["UDate"]));
					$smarty->assign('id', $ds["ID"]);
					$smarty->display('w_info.tpl');
				}
			} else {
				echo 'Keine Dateien vorhaanden';
			}
			if($sql = mysql_fetch_assoc(mysql_query("SELECT * FROM tg_w_cat_u WHERE ID = '".$id."'"))) {
				echo '<br /><br /><a href="ucat_'.$sql["UCat"].'.html" class="back">Zur�ck</a>';
			} else {
				echo '<br /><br /><a href="cat.html" class="back">Zur�ck</a>';
			}		
		} else {
			moveto('index.php', 3, 'Error:<br /><dd>ID Error</dd>');
		}
	 } elseif($mode == 'file') {
		if(!empty($_GET["fileID"])) {
			$id = $_GET["fileID"];
			$con = mysql_query("SELECT * FROM tg_w_info WHERE ID = '".$id."' AND Activ = '1'");
			$bb = new BBCode(1);
			if(mysql_num_rows($con)) {
				$ds = mysql_fetch_assoc($con);
				$da = mysql_fetch_assoc(mysql_query("SELECT * FROM tg_w_cat_u WHERE ID = '".$ds["Cat"]."'"));
				$px = str_replace("px", "", $da["Cat"]);
				$text->h4($px."px Texturenpacks:");
				$smarty->assign('titel', $ds["Titel"]);
				$smarty->assign('px', $px."x".$px." Pixel");
				$smarty->assign('up', $ds["Uploader"]);
				$smarty->assign('size', $ds["Size"]);
				$smarty->assign('time', date('d.m.Y - H:i', $ds["UDate"]));
				$smarty->assign('info', $bb->BBParser($ds["Info"]));
				$smarty->assign('links', $bb->BBParser($ds["FLinks"]));
				$smarty->display('w_file.tpl');
				if($ds["Uploader"] == getusername($_SESSION["tg_userID"])) {
					echo '<br /><a href="#">Texturepack Editieren</a><br />';
				}
				echo back();
			} else {
				moveto('index.php?content=wcat', 3, 'Error:<br /><dd>ID Error</dd>');
			}
		}
	 } elseif($mode == 'add') {
		if(checkLogin()) {
			if(!empty($_GET["cat"])) {
				if(empty($_POST["s_add"])) {
					$smarty->assign('error', '');
					$smarty->assign('dive', '');
					$smarty->assign('dive2', '');
					$cat = $_GET["cat"];
					$con = mysql_fetch_array(mysql_query("SELECT ID, Cat, UCat FROM tg_w_cat_u WHERE ID = '".$cat."'"));
					$ds = mysql_fetch_assoc(mysql_query("SELECT Cat FROM tg_w_cat WHERE ID = '".$con["UCat"]."'"));
					$smarty->assign('cat', 'Texturenpack hinzuf�gen (<a href="index.php?content=files&cat='.$con["ID"].'">'.$con["Cat"].'</a>)');
					$smarty->assign('catID', $con["ID"]);
					$smarty->assign('catUID', $cat);
					$smarty->display('w_add.tpl');
				} else {
					$error = array();
					if(empty($_POST["file"])) {
						$error[] = 'Bitte gebe ein Titel an<br />';
					} else {
						$file = $_POST["file"];
					}
					if(empty($_POST["mes"])) {
						$error[] = 'Bitte gebe Informationen zum File an!<br />';
					} else {
						$mes = $_POST["mes"];
					}
					if(empty($_POST["size"])) {
						$error[] = 'Bitte gebe ein Gr��e an<br />';
					} else {
						$size = $_POST["size"];
					}
					if(empty($_POST["dl"])) {
						$error[] = 'Bitte gebe die Download links an<br />';
					} else {
						$dl = $_POST["dl"];
					}
					if(!empty($error)) {
						$cat = $_GET["cat"];
						$con = mysql_fetch_array(mysql_query("SELECT ID, Cat, UCat FROM tg_w_cat_u WHERE ID = '".$cat."'"));
						$ds = mysql_fetch_assoc(mysql_query("SELECT Cat FROM tg_w_cat WHERE ID = '".$con["UCat"]."'"));
						$smarty->assign('error', $error);
						$smarty->assign('dive', '<div class="lerror">');
						$smarty->assign('dive2', '</div>');
						$smarty->assign('cat', 'Texturenpack hinzuf�gen (<a href="index.php?content=files&cat='.$con["ID"].'">'.$con["Cat"].'</a>)');
						$smarty->display('w_add.tpl');
					} else {
						$con = mysql_query("INSERT INTO tg_w_info (Cat, Titel, Uploader, Size, UDate, Info, FLinks, Activ) VALUES ('".$_POST["catUID"]."', '".$file."', '".getusername($_SESSION["tg_userID"])."', '".$size."', '".time()."', '".$mes."', '".$dl."', '1')");
						if($con) {
							moveto('ucat_1.html', 3, 'File wurde hinzugef�gt!');
						} else {
							moveto('index.php?content=wcat', 3, 'File wurde nicht hinzugef�gt!<br />'.mysql_error());
						}
					}
				}
			}
		} else {
			echo '<div class="lerror">Bitte Logge dich ein um diese Funktion nutzen zuk�nnen</div>';
		}
	}
?>