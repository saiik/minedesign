<?php
/*******************************************
 *#########################################*
 *##|_   _|   __|___|     |     |   __|####*
 *####| | |  |  |___|   --| | | |__   |####*
 *####|_| |_____|   |_____|_|_|_|_____|####*
 *#########################################*
 *  (C) by TG-Network and sLy(Tobias F.)   *
 * 		    This is a Free CMS             *
 *  Dont remove this.      (C) 2009-2010   *
 *******************************************/
 
	$mode = getMode('mode');
	$safe = new Safe();
	$text = new Text();
	function autoload($class) {
		include('config/class/'.$class.'.php');
	}
	
	$xml = simplexml_load_file('config/xml/news.xml');
	$x_news = $xml->config->maxnews;
	
	if(empty($mode)) {
		$smarty = new Smarty();
		$smarty->template_dir = './templates/'.template().'/templates';
		$smarty->compile_dir = './templates/'.template().'/templates_c';
		$con = mysql_query("SELECT * FROM tg_news ORDER BY ID DESC LIMIT 0,".$x_news);
		$bb = new BBCode(1);
		if(mysql_num_rows($con)) {
			while($ds = mysql_fetch_assoc($con)) {
				$anz = mysql_query("SELECT * FROM tg_news_com WHERE newsID = '".$ds["ID"]."'");
				if($all = mysql_num_rows($anz)) {
					if($all > 1) {
						$com = $all.' Kommentare';
					} else {
						$com = $all.' Kommentar';
					}
				} else {
					$com = 'Keine Kommentare';
				}
				$smarty->assign('titel', $ds["Titel"]);
				$smarty->assign('id', $ds["ID"]);
				$smarty->assign('com', $com);
				$smarty->assign('Text', $bb->BBParser($ds["NText"]));
				$smarty->assign('user', $ds["User"]);
				$smarty->assign('Datum', date("d.m.Y - H:i", $ds["Datum"]));
				if(empty($ds["Q1"]) || empty($ds["Q2"]) || empty($ds["Q3"])) {
					$smarty->assign('q', 'Keine Quelle vorhanden');
				} else {
					if(empty($ds["Q2"])) {
						$smarty->assign('q', '<a href="'.$ds["Q1"].'">'.$ds["Q1T"].'</a>');
					} else {
						if(empty($ds["Q3"])) {
							$smarty->assign('q', '<a href="'.$ds["Q1"].'">'.$ds["Q1T"].'</a> <a href="'.$ds["Q2"].'">'.$ds["Q2T"].'</a>');
						} else {
							$smarty->assign('q', '<a href="'.$ds["Q1"].'">'.$ds["Q1T"].'</a> <a href="'.$ds["Q2"].'">'.$ds["Q2T"].'</a> <a href="'.$ds["Q3"].'">'.$ds["Q3T"].'</a>');
						}
					}
				}
				$smarty->display('news.tpl');
			}
		} else {
			$text->h4("Keine News vorhanden!");
		}
	} elseif($mode == 'com') {
		$action = getMode('action');
		if($action == 'add') {
			if(!empty($_POST["c_add"])) {
				if(!empty($_POST["message"])) {
					if(!empty($_POST["newsID"])) {
						$con = mysql_query("INSERT INTO tg_news_com (newsID, CDate, CText, userID) VALUES ('".$_POST["newsID"]."', '".time()."', '".$_POST["message"]."', '".$_SESSION["tg_userID"]."')");
						if($con) {
							moveto('index.php?content=news&mode=archive&id='.$_POST["newsID"], 3, '<div class="success">Kommentar wurde hinzugef�gt!<br /><a href="index.php?content=news&mode=archive&id='.$_POST["newsID"].'">Klicken Sie hier um Weitergeleitet zuwerden</a></div>');
						} else {
							moveto('index.php?content=news&mode=archive&id='.$_POST["newsID"], 3, '<div class="lerror">Kommentar wurde nicht hinzugef�gt<br /><a href="index.php?content=news&mode=archive&id='.$_POST["newsID"].'">Klicken Sie hier um Weitergeleitet zuwerden</a></div>');
						}
					} else {
						moveto('index.php?content=news', 3, '<div class="lerror">News ID Error</div>');
					}
				} else {
					moveto('index.php?content=news', 3, '<div class="lerror">Message Error</div>');
				}
			} else {
				moveto('index.php?content=news', 3, '<div class="lerror">Form Error</div>');
			}
		} elseif($action == 'del') {
			if(!empty($_GET["comID"])) {
				if($_GET["hashID"] == $_SESSION["tg_del_n_hashID"]) {
					if(mysql_num_rows(mysql_query("SELECT NAdmin FROM tg_admin_group WHERE userID = '".$_SESSION["tg_userID"]."' AND NAdmin = '1'"))) {
						$id = $_GET["comID"];
						$newsID = mysql_fetch_assoc(mysql_query("SELECT newsID FROM tg_news_com WHERE comID ='".$id."'"));
						$con = mysql_query("DELETE FROM tg_news_com WHERE comID = '".$id."'");
						if($con) {
							moveto('index.php?content=news&mode=archive&id='.$newsID["newsID"], 3, '<div class="success">Kommentar wurde gel�scht<br /><a href="index.php?content=news&mode=archive&id='.$newsID["newsID"].'">Klicken Sie hier um Weitergeleitet zuwerden</a></div>');
						} else {
							moveto('index.php?content=news&mode=archive&id='.$newsID["newsID"], 3, '<div class="lerror">Kommentar wurde nicht gel�scht<br /><a href="index.php?content=news&mode=archive&id='.$newsID["newsID"].'">Klicken Sie hier um Weitergeleitet zuwerden</a></div>');
						}
					} else {
						moveto('index.php?content=news', 3, 'Hash ID Error');
					}
				} else {
					moveto('index.php?content=news', 3, 'comID Error');
				}
			}
		}
	} elseif($mode == 'archive') {
		if(!empty($_GET["id"])) {
		
			$smarty = new Smarty();
			$smarty->template_dir = './templates/'.template().'/templates';
			$smarty->compile_dir = './templates/'.template().'/templates_c';
			if(!empty($_GET["id"])) {
				$id = $_GET["id"];
				$con = mysql_query("SELECT * FROM tg_news WHERE ID = '".$id."'");
				if(mysql_num_rows($con)) {
					$bb = new BBCode(1);
					$ds = mysql_fetch_assoc($con);
					$anz = mysql_query("SELECT * FROM tg_news_com WHERE newsID = '".$ds["ID"]."'");
					if($all = mysql_num_rows($anz)) {
						if($all > 1) {
							$com = $all.' Kommentare';
						} else {
							$com = $all.' Kommentar';
						}
					} else {
						$com = 'Keine Kommentare';
					}
					$smarty->assign('titel', $ds["Titel"]);
					$smarty->assign('text', $bb->BBParser($ds["NText"]));
					$smarty->assign('user', $ds["User"]);
					$smarty->assign('id', $ds["ID"]);
					$smarty->assign('com', $com);
					$smarty->assign('Datum', date("d.m.Y - H:i", $ds["Datum"]));
					if(empty($ds["Q1"]) || empty($ds["Q2"]) || empty($ds["Q3"])) {
						$smarty->assign('q', 'Keine Quelle vorhanden');
					} else {
						if(empty($ds["Q2"])) {
							$smarty->assign('q', '<a href="'.$ds["Q1"].'">'.$ds["Q1T"].'</a>');
						} else {
							if(empty($ds["Q3"])) {
								$smarty->assign('q', '<a href="'.$ds["Q1"].'">'.$ds["Q1T"].'</a> <a href="'.$ds["Q2"].'">'.$ds["Q2T"].'</a>');
							} else {
								$smarty->assign('q', '<a href="'.$ds["Q1"].'">'.$ds["Q1T"].'</a> <a href="'.$ds["Q2"].'">'.$ds["Q2T"].'</a> <a href="'.$ds["Q3"].'">'.$ds["Q3T"].'</a>');
							}
						}
					}
					$smarty->display('news_a.tpl');
					$text->br("2");
					$text->h4("News Kommentare:");
					$sql = mysql_query("SELECT * FROM tg_news_com where newsID = '".$id."' ORDER BY comID DESC");
					$hash = $safe->generateHash("tg_del_n_hashID");
					if(mysql_num_rows($sql)) {
						while($dr = mysql_fetch_assoc($sql)) {
							if(checkLogin()) {
								$new = mysql_query("SELECT NAdmin FROM tg_admin_group WHERE userID = '".$_SESSION["tg_userID"]."' AND NAdmin = '1'");
								if(mysql_num_rows($new)) {
									$smarty->assign('admin', '<br /><a href="index.php?content=news&mode=com&action=del&comID='.$dr["comID"].'&hashID='.$_SESSION["tg_del_n_hashID"].'">L�schen</a>');
								} else {
									$smarty->assign('admin', '');
								}
							}
							$smarty->assign('user', getusername($dr["userID"]));
							$smarty->assign('date', date('d.m.Y - H:i', $dr["CDate"]));
							$smarty->assign('text', $bb->BBParser($dr["CText"]));
							$smarty->display('news_com.tpl');
						}
						$text->br();
					} else {
						echo 'Keine Kommentare vorhanden';
					}
					if(checkLogin()) {
						$smarty->display('news_com_add.tpl');
					}
				}
			} else {
				close('XSS / SQLi Schutz Aktiviert!', 1);
			}
		} elseif($_GET["id"] == '-1') {
			close('XSS / SQLi Schutz Aktiviert!', 1);
		} else {
			close('XSS / SQLi Schutz Aktiviert!', 1);
		}
	}
?>